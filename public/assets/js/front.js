/**
 * Created by Jack Bui on 1/12/2016.
 */
(function($) {
    //Scroll modal in step 2
    $('.modal').on('shown.bs.modal', function (e) {
        $('body').addClass('modal-open');
    });
    $(document).ready(function() {

        var slider = $("#slider").mostSlider({
            aniMethod: 'auto',
        });
       
        $('._10nar-pull-bottom').css("display","block");
        /*Show tooltip*/
        $('[data-toggle="tooltip"]').tooltip();

        /*Show white space two line*/
        $(".trimmed").dotdotdot({
          watch: true,
          wrap: 'letter'
        });
        $(".user-public-profile .list-job ul li .info-items h2").dotdotdot({
            watch: true,
            wrap: 'letter'
        });

        /*Scroll custom conversation*/
        $msg_container_height = $('.conversation-form .list-conversation').height();
        $(".wrapper-list-conversation").mCustomScrollbar({
            theme:"minimal",
            callbacks:{
                onInit:function(){
                    $('#mCSB_1_container').css({
                        top: -$msg_container_height
                    })
                }
            }
        });


        /*Validation input text time*/
        $('.field-positive-int, .time-delivery, #from-budget, #from_deadline, #budget, #etd').on('keypress',function(event) {
            var keypressed = null;
            if (window.event)
            {
                keypressed = window.event.keyCode;
            }
            else
            {
                keypressed = event.which;
            }

            if (keypressed < 48 || keypressed > 57)
            {
                if (keypressed == 8 || keypressed == 127)
                {
                    return;
                }
                return false;
            }
        });
		
		jQuery(".order_person").change(function(){
			if(jQuery(this).is(':checked') && jQuery(this).hasClass('order_seller')){
				jQuery(".select_order_change").fadeOut();
			}
			else{
				jQuery(".select_order_change").fadeIn();
			}
		})
		
        /*Focus searh form*/
        $(".new-search-link").click(function() {
            $("#input-search").select();
            $('html, body').animate({
                scrollTop: $("html, body").offset().top
            }, 1000);
        });

        /*Scroll link statistic job home*/
        $(".link-last-job").click(function() {
            $('html, body').animate({
                scrollTop: $(".block-items").offset().top
            }, 1000);
        });

        $("._10nar-pull-top ,#content, .slider").click(function() {
            $('.navbar-collapse').removeClass('in');
            $('.navbar-collapse').addClass('collapsed');
        });
        /*Hover dropdown menu*/
        var windowSize = $(window).width();
        if(windowSize > 992){
            $('#_10nar-nav .dropdown').hover(function(){
                $('.dropdown-toggle', this).trigger('click');
            });
        }
        /*Scroll header home*/
        $(window).on('scroll',function(){
            //scroll header
            var form_search = $(".form-search");
            if(form_search.length){
                var form_search_top = form_search.offset().top;
                if($(window).scrollTop() > form_search_top){
                    $('.search-bar').css("display", "inline-block");
                }else{
                    $('.search-bar').css("display", "none");
                }
            }
        });
        /*Check show arrows if two image*/
        var count = $(".carousel-indicators li").length;
        if(count <= 1){
            $('.carousel-indicators').hide();
            $('.carousel-control').hide();
        }else{
            $('.carousel-indicators').show();
            $('.carousel-control').show();
        }

        /*Menu navigation long*/
        navigationResize();
        $(window).resize(function() {
          navigationResize();
        });

        function navigationResize() {
            var $navItemMore = $('#nav > li.more'),
            $navItems = $('#nav > li:not(.more)'),
            navItemWidth = $navItemMore.width(),
            windowWidth = $(window).width();
            if( window.innerWidth > 991 ) {
                //$('#nav li.more').before($('#overflow > li'));

                $navItems.each(function() {
                    navItemWidth += $(this).width();
                });

                navItemWidth > windowWidth ? $navItemMore.show() : '';

                while (navItemWidth > windowWidth) {
                    navItemWidth -= $navItems.last().width();
                    $navItems.last().prependTo('#overflow');
                    $navItems.splice(-1,1);
                }
            } else {
                navItemWidth > windowWidth ? $navItemMore.show() : $navItemMore.hide();
            }
        }
        $(".navbar-nav li").hover( function (e) {
            if ($('ul', this).length) {
                var docW = $("body").width();
                var liRight = docW - ($(this).width() + $(this).offset().left);
                var ulWidth = $(this).find('.dropdown-menu').width();
                var isEntirelyVisible = (ulWidth > liRight);
                if (isEntirelyVisible) {
                    $(this).addClass('move-right');
                    $(this).find('.dropdown-menu').css({right:liRight});
                }
            }
        });
        $('#_10nar-nav .navbar-toggle').click(function(event){
            event.preventDefault();
            $('.overlay-nav').show();
            $('body').css('cssText','overflow:hidden !important; position:fixed');
            $('#_10nar-nav .navbar-collapse.collapse').show();
        });
        $('.overlay-nav').click(function(){
            $('.overlay-nav').hide();
            $('#_10nar-nav .navbar-collapse.collapse').hide();
            $('body').css('cssText','overflow:auto !important; position:relative');
        });

        /*Animation scroll*/
        wow = new WOW(
            {
                animateClass: 'animated',
                offset:       100,
            }
        );
        wow.init();


     

        /**
         * model mjob
         */
        /*Fixed height banner home*/
        var header = $('._10nar-pull-bottom').outerHeight();
        var admin_bar = $('body').hasClass('admin-bar');
        var wpadminbar = 0;
        if(admin_bar) {
            wpadminbar = $('#wpadminbar').outerHeight();
            if($('#wpadminbar').hasClass('mobile') && ($('#wpadminbar').outerWidth() < 992)) {
                wpadminbar = 0
            }
        }
        $(window).resize(function(event) {
            var header = $('._10nar-pull-bottom').outerHeight();
            var wpadminbar = 0;
            if(admin_bar) {
                wpadminbar = $('#wpadminbar').outerHeight();
                if($('#wpadminbar').hasClass('mobile') && ($('#wpadminbar').outerWidth() < 992)) {
                    wpadminbar = 0
                }
            }
            jQuery('.slider').css({
                height: (jQuery(window).height() - 85 - header - wpadminbar) + 'px',
                'display':'block'
            });
        });

        jQuery('.slider').css({
            height: (jQuery(window).height() - 85 - header - wpadminbar) + 'px',
            'display':'block'
        });
        /*Accordion menu*/
        var Accordion = function(el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;

            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }

        Accordion.prototype.dropdown = function(e) {
            var $el = e.data.el;
            $this = $(this),
                $next = $this.next();

            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            };
        }
        var accordion = new Accordion($('#accordion'), false);

        // Prevent dropdown menu hidden when click on accordion
        $('.dropdown').on('click', '.show-accordion', function(event) {
            event.preventDefault();
            event.stopPropagation();
        });

       
 
    })
})(jQuery);

