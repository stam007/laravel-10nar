<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'firstname', 'lastname', 'password', 'company_name', 'tax_registration', 'account_type', 'wish', 'avatar', 'balance', 'status', 'phone', 'is_email_verified', 'email_token', 'is_email_sent', 'address', 'city', 'postal_code', 'country'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  public function posts()
  {
    return $this->hasMany('App\Post');
  }

  public function comments()
  {
    return $this->hasMany('App\Comment');
  }

  public function services()
  {
    return $this->hasMany('App\Service');
  }

  public function userOrderMessages()
  {
    return $this->hasMany('App\UserOrderMessage');
  }

  public function withdraws()
  {
    return $this->hasMany('App\Withdraw');
  }

  public function deposits()
  {
    return $this->hasMany('App\Deposit');
  }
}
