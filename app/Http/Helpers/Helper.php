<?php
use App\GeneralSettings as Gsetting;
use App\Ad;

if (! function_exists('send_email')) {

    function send_email( $to, $name, $subject, $message)
    {
        $settings = Gsetting::first();
        $template = $settings->email_template;
        $from = $settings->email_sent_from;
		// if($settings->email_notification == 1)
		// {
    /*
		$headers = "From: $settings->website_title <$from> \r\n";
		$headers .= "Reply-To: $settings->website_title <$from> \r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
*/
		$mm = str_replace("{{name}}",$name,$template);
//		$message = str_replace("{{message}}",$message,$mm);
$html_message = str_replace("{{message}}",$message,$mm);

        //$blade_from_db = view('your view',['something' => 'World!'])->render();

        /*
        mail($to, $subject, $message, $headers);
        */

        // }
        
        $data = array(
            'from' => $settings->email_sent_from,
            'platformTitle' => $settings->website_title,
            'to' => $to,
            'name' => $name,
            'subject' => $subject
    );

        Mail::send([], $data, function($message) use ($data, $html_message) {
           $message->to($data['to'], $data['name'])->subject
              ($data['subject']);
           $message->from($data['from'],$data['platformTitle']);
           $message->setBody($html_message, 'text/html');;

        });

    }
}

if(!function_exists('show_ad')) {
    function show_ad($size) {
        $ad = Ad::where('size', $size)->where('type', 1)->inRandomOrder()->get();
        if(!empty($ad)) {
            return $ad;
        } else {
            $ad = Ad::where('size', $size)->where('type', 2)->inRandomOrder()->get();
            return $ad;
        }
    }
}