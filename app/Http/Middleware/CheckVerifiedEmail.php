<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckVerifiedEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if(Auth::check() && Auth::user()->is_email_verified != '1'){
            Auth::logout();
            return redirect('/connexion')->withErrors('Vous devez confirmer votre adresse e-mail avant d’effectuer cette action.');
        }

        return $response;
    }
}
