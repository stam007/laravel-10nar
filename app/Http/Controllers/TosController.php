<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GeneralSettings as GS;
use Session;

class TosController extends Controller
{
  public function __construct()
  {
    $gs = GS::first();
    $this->sitename = $gs->website_title;
  }

  public function index()
  {
    $data['sitename'] = $this->sitename;
    $data['page_title'] = 'Terms & Conditions';
    return view('admin.tos.index', ['data' => $data]);
  }

  public function update(Request $request)
  {

    $messages = [
      'tos.required' => 'Terms & Conditions field is required'
    ];

    $validatedRequest = $request->validate([
      'tos' => 'required'
    ], $messages);

    $gs = GS::first();
    $gs->tos = $request->tos;
    $gs->save();
    Session::flash('success', 'Terms and Conditions updated successfully!');

    return redirect()->back();
  }

  public function cgu() {
    $data['sitename'] = $this->sitename;
    $data['page_title'] = 'Terms & Conditions';
    return view('cgu', ['data' => $data]);
  }
}
