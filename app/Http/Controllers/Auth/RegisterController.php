<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\GeneralSettings;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Session;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/connexion';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'tos.required' => 'Vous devez accepter nos conditions générales afin d\'être inscrit(e) sur le site.'
        ];

        return Validator::make($data, [
            'name' => 'required|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'firstname' => 'required',
            'lastname' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'account_type' => 'required',
            'company_name' => 'required_if:account_type,Entreprise Individuelle|required_if:account_type,Société',
            'tax_registration' => 'required_if:account_type,Entreprise Individuelle|required_if:account_type,Société',
            'wish' => 'required',
            'tos' => 'required'
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $gen = GeneralSettings::first();

        // if (config('app.registration') == 1) {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'password' => bcrypt($data['password']),
            'account_type' => $data['account_type'],
            'company_name' => $data['company_name'],
            'tax_registration' => $data['tax_registration'],
            'wish' => $data['wish'],
            'email_token' => sha1(time())
        ]);
        //        }
    }

    //This will override the register method.
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        if ($user->id){
            try {
                $url = url("/inscription/confirmation/{$user->email_token}");
                $content  = "Vous avez créé un compte sur <a href='https://10nar.net' target='_blank'>10nar.net</a><br/><br/>";
                $content .= 'Pour l\'activer, veuillez cliquer sur le lien ci-dessous ou le copier/coller dans votre navigateur internet :<br/><br/>';
                $content .= "<a href='{$url}' target='_blank'>{$url}</a>";
                send_email($user->email, $user->firstname, 'Confirmez votre inscription', $content);
                $user->is_email_sent= 1;
                $user->save();
            } catch (\Exception $e) {
                \log::error($e->getMessage());
            }
        }

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->withSuccess("Félicitation! Vous y êtes presque! Un mail d'activation vous a été envoyé, veuillez vérifier votre boite de réception ou spam.");
    }


    public function showRegistrationForm($username = null)
    {
        return view('auth.register')->with('username');
    }
}
