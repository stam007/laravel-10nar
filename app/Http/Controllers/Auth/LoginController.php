<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest', 'checkstatus', 'checkverifiedemail'])->except('logout');
    }

    /*
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }*/

    public function confirmSubcription($token)
    {
        $user = User::where('email_token', $token)->first();
        if (!$user || ($user && $user->is_email_verified == 1)) {
            return redirect()->route('login')->withErrors("Oups, le lien n'est pas (ou n'est plus) valide. Veuillez contacter le service de support de 10NAR.");
        }
        if ($user && $user->is_email_verified == 0) {
            $user->is_email_verified = 1;
            $user->save();
            Auth::login($user);
            return redirect()->intended($this->redirectPath());
        }
        return redirect()->route('login')->withErrors("Oups, une erreur est survenue. Veuillez contacter le service de support de 10NAR.");
    }
}
