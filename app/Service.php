<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Service extends Model
{
  use Sluggable;

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'service_title',
        'unique' => false
      ]
    ];
  }

  public function category()
  {
    return $this->belongsTo('App\Category');
  }

  public function subcategory()
  {
    return $this->belongsTo('App\Subcategory');
  }

  public function tags()
  {
    return $this->hasMany('App\Tag');
  }

  public function serviceImages()
  {
    return $this->hasMany('App\ServiceImage');
  }

  public function serviceOptions()
  {
    return $this->hasMany('App\ServiceOption');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function orders()
  {
    return $this->hasMany('App\Order');
  }
}
