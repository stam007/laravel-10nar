<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Subcategory extends Model
{
  use Sluggable;

  protected $fillable = ['category_id', 'name'];

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'title'
          ]
      ];
  }

  
  public function services()
  {
    return $this->hasMany('App\Service');
  }
}
