<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOption extends Model
{
    public function service() {
      return $this->blongsTo('App\Service');
    }
}
