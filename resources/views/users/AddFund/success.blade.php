@extends('users.layout.master')

@section('title', 'Deposit Success')

@section('content')
  <div class="jumbotron">
      <h1>You have successfully added fund to your account.</h1>
  </div>
@endsection
