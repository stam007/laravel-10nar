@extends('layouts.app')

@push('after-scripts')
@endpush

@push('styles')
<script type="text/javascript"
  src="https://platform-api.sharethis.com/js/sharethis.js#property=5f23283da1fb790012a0a9ac&product=inline-share-buttons"
  async="async"></script>

<style type="text/css">
  .steps {
    list-style: none;
    display: flex;
    padding-inline-start: 0px;
  }

  .steps .step {
    white-space: nowrap;
    transition: 0.3s ease-in-out;
    background: #6ACBDA;
    position: relative;
    height: 50px;
    line-height: 50px;
    margin-right: 30px;
    padding: 0 20px;
  }

  .steps .step>a {
    text-decoration: none;
    color: white;
  }

  .steps .step:last-child {
    margin-right: 0;
  }

  .steps .step::before {
    transition: 0.3s ease-in-out;
    content: "";
    position: absolute;
    top: 0;
    left: -25px;
    border-left: 25px solid transparent;
    border-top: 25px solid #6ACBDA;
    border-bottom: 25px solid #6ACBDA;
  }

  .steps .step:first-child::before {
    display: none;
  }

  .steps .step::after {
    transition: 0.3s ease-in-out;
    position: absolute;
    top: 0;
    right: -25px;
    border-left: 25px solid #6ACBDA;
    border-top: 25px solid transparent;
    border-bottom: 25px solid transparent;
    content: "";
  }

  .steps .step:last-child::after {
    display: none;
  }

  .steps .step:hover {
    background: #2993d1;
  }

  .steps .step:hover::before {
    border-top-color: #2993d1;
    border-bottom-color: #2993d1;
  }

  .steps .step:hover::after {
    border-left-color: #2993d1;
  }

  .steps .step:first-child {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
  }

  .steps .step:last-child {
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
  }
</style>

@endpush

@section('meta-ajax')
<meta name="_token" content="{{ csrf_token() }}" />
@endsection

@section('title', auth()->user()->name." :: Je vais {$service->service_title} pour 10 dinars | ".$gs->website_title )

@section('content')
<div id="content" class="mjob-single-page">

  <div class="container mjob-single-primary">
    <div class="row">
      <div class="col-md-12">
        <ul class="steps">
          <li class="step">
            <a href="/">
              <i class="fas fa-home" aria-hidden="true"></i>
            </a>
          </li>
          <li class="step">
            <a href="{{"/categorie/{$service->category->slug}"}}">
              {{$service->category->name}}
            </a>
          </li>
          <li class="step">
            <a href="{{"/categorie/{$service->category->slug}/{$service->subcategory->slug}"}}">
              {{$service->subcategory->name}}
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="mjob-single-content box-shadow">
          <div class="mjob-single-header mjob-single-block pad-lr-30">
            <h2>
              <span class="rendered-text">
                {{"Je vais {$service->service_title} pour 10 dinars."}}
              </span>
            </h2>
            <div class="mjob-single-meta clearfix">
              <span class="pull-left">
                <div class="sharethis-inline-share-buttons"></div>
              </span>

              <span class="time-post pull-right">
                Crée le <span><i class="far fa-calendar-alt"></i>
                  <?php
                  \Carbon\Carbon::setLocale(config('app.locale'));
                  echo \Carbon\Carbon::parse($service->created_at)->format('d/m/Y')
                  ?>
                </span>
              </span>
            </div>
          </div><!-- end .mjob-single-header -->

          <div class="mjob-single-gallery ">
            <div class="gallery">

              @if (count($service->serviceImages) == 1)
              <img src="{{asset('uploads/microservices/' . $service->serviceImages->first()->image_name)}}" alt="">
            @else
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  @for ($i=0; $i < count($service->serviceImages); $i++)
                    <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" {{$i==0 ? 'class="active"' : ''}}></li>
                  @endfor
                </ol>
    
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  @foreach ($service->serviceImages as $serviceImage)
                    <div class="item {{($loop->first) ? 'active' : ''}}">
                      <img style="width:100%" src="{{asset('_assets/users/service_images/' . $serviceImage->image_name)}}" alt="...">
                    </div>
                  @endforeach
                </div>
    
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Précédent</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Suivant</span>
                </a>
              </div>
            @endif
            </div>
          <div class="mjob-single-description mjob-single-block pad-lr-30">
            <h3 class="title">Description</h3>
            <div class="post-detail description">
              <div class="blog-content">
                <div class="post-content">
                  @if($service->video_url)
                    <iframe width="100%" height="315" src="{{$service->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  @endif
                  {!!$service->description!!}

                </div>
              </div>
              <!--
              <div class="mjob-single-description mjob-single-block">
                <h3 class="title">Options supplémentaires</h3>
                <div class="post-detail description">
                  <div class="blog-content">
                    <table class="tableOptions">
                      <tbody>
                        <tr>
                          <td style="padding-right: 10px;" valign="top">
                            <input type="checkbox" id="cart_extras_198004" name="cart[extras][]" data-value="5"
                              data-delay="" data-toggle="price" data-target="span.price-placeholder-92471"
                              value="198004">
                          </td>
                          <td width="100%">
                            <label class="tableOption-label" for="cart_extras_198004">
                              20 produits gagnants en tout
                              <span style="display: block;" class="extra-delay">

                                Pas de délai supplémentaire

                              </span>
                            </label>
                          </td>
                          <td style="white-space: nowrap;" valign="top" align="right">
                            <span class="extra-price">+ 5&nbsp;€</span>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding-right: 10px;" valign="top">
                            <input type="checkbox" id="cart_extras_224455" name="cart[extras][]" data-value="10"
                              data-delay="" data-toggle="price" data-target="span.price-placeholder-92471"
                              value="224455">
                          </td>
                          <td width="100%">
                            <label class="tableOption-label" for="cart_extras_224455">
                              30 produits gagnants en tout
                              <span style="display: block;" class="extra-delay">

                                Pas de délai supplémentaire

                              </span>
                            </label>
                          </td>
                          <td style="white-space: nowrap;" valign="top" align="right">
                            <span class="extra-price">+ 10&nbsp;€</span>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding-right: 10px;" valign="top">
                            <input type="checkbox" id="cart_extras_224456" name="cart[extras][]" data-value="15"
                              data-delay="" data-toggle="price" data-target="span.price-placeholder-92471"
                              value="224456">
                          </td>
                          <td width="100%">
                            <label class="tableOption-label" for="cart_extras_224456">
                              40 produits gagnants en tout
                              <span style="display: block;" class="extra-delay">

                                Pas de délai supplémentaire

                              </span>
                            </label>
                          </td>
                          <td style="white-space: nowrap;" valign="top" align="right">
                            <span class="extra-price">+ 15&nbsp;€</span>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding-right: 10px;" valign="top">
                            <input type="checkbox" id="cart_extras_224457" name="cart[extras][]" data-value="20"
                              data-delay="" data-toggle="price" data-target="span.price-placeholder-92471"
                              value="224457">
                          </td>
                          <td width="100%">
                            <label class="tableOption-label" for="cart_extras_224457">
                              50 produits gagnants en tout
                              <span style="display: block;" class="extra-delay">

                                Pas de délai supplémentaire

                              </span>
                            </label>
                          </td>
                          <td style="white-space: nowrap;" valign="top" align="right">
                            <span class="extra-price">+ 20&nbsp;€</span>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding-right: 10px;" valign="top">
                            <input type="checkbox" id="cart_extras_224458" name="cart[extras][]" data-value="25"
                              data-delay="" data-toggle="price" data-target="span.price-placeholder-92471"
                              value="224458">
                          </td>
                          <td width="100%">
                            <label class="tableOption-label" for="cart_extras_224458">
                              60 produits gagnants en tout
                              <span style="display: block;" class="extra-delay">

                                Pas de délai supplémentaire

                              </span>
                            </label>
                          </td>
                          <td style="white-space: nowrap;" valign="top" align="right">
                            <span class="extra-price">+ 25&nbsp;€</span>
                          </td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            -->
            </div>
            <div class="clearfix">
              <div class="tags">
                <div class="list-require-skill-project list-taxonomires list-skill">
                <ul>
              @foreach ($service->tags as $tag)
                <li><span class="badge">{{$tag->name}}</span></li>
              @endforeach
                </ul>
                </div>
              </div>
            </div>
              <button class="btn-order waves-effect waves-light btn-diplomat mjob-order-action">
                Commander (<span class="mjob-price mje-price-text"><span title="10"><span
                      class="mje-price">10<sup>TND</sup></span></span></span>) </button>
            </div>
          </div><!-- end .mjob-single-description -->
        </div><!-- end .mjob-single-content -->

        <div class="mjob-single-content box-shadow">
          <div class="mjob-single-review mjob-single-block pad-lr-30">
            <div class="review-job">
              <h3 class="title">
                Commentaires <span class="total-review">(0 total)</span> </h3>
<!--
              <ul>
                <li id="review-7" class="review-item clearfix">
                  <div class="image-avatar">
                    <img src="https://cdn.enginethemes.com/microjobengine/2016/03/cropped-10.jpg" class="avatar" alt="">
                  </div>
                  <div class="profile-viewer">
                    <a href="https://microjobengine.enginethemes.com/microjobengine-diplomat/author/edenwaller/"
                      class="name-author md-opjjpmhoiojifppkkcdabiobhakljdgm_doc">
                      Eden Waller </a>
                    <p class="review-time">March 11, 2016</p>
                    <div class="rate-it star" data-score="3" title="nice"><i data-alt="1"
                        class="fas fa-star star-on-png" title="nice"></i>&nbsp;<i data-alt="2"
                        class="fas fa-star star-on-png" title="nice"></i>&nbsp;<i data-alt="3"
                        class="fas fa-star star-on-png" title="nice"></i>&nbsp;<i data-alt="4"
                        class="fas fa-star-o off star-off-png" title="nice"></i>&nbsp;<i data-alt="5"
                        class="fas fa-star-o off star-off-png" title="nice"></i><input name="score" type="hidden"
                        value="3" readonly=""></div>
                    <div class="commnet-content">
                      <p>Wow, you’re doing a great job with MicrojobEngine! Keep up the good work!</p>
                    </div>
                  </div>
                </li>
              </ul>
            -->
            <div class="review-item clearfix">Aucun commentaire. Soyez le premier à commenter ce vendeur.</div>

              <div class="paginations-wrapper">
              </div>
            </div>
          </div><!-- end .mjob-single-review -->
        </div>

        <div class="mjob-edit-content">
          <form class="post-job step-post post et-form edit-mjob-form" style="display: none">
            <p class="mjob-title">Edit your job</p>

            <div class="loading">
              <div class="loading-img"></div>
            </div>

            <div class="form-group clearfix">
              <div class="input-group">
                <label for="post_title" class="input-label">Job name</label>
                <input type="text" class="input-item input-full" name="post_title" value="" required="">
              </div>
            </div>
            <div class="form-group row clearfix has-price-field">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 clearfix">
                <div class="input-group">
                  <label for="et_budget">Price (USD)</label>
                  <input type="number" name="et_budget" placeholder="$1,00 - $10,00"
                    class="input-item et_budget field-positive-int time-delivery" min="1" max="10"
                    pattern="[-+]?[0-9]*[.,]?[0-9]+" required="">
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 delivery-area">
                <div class="input-group delivery-time">
                  <label for="time_delivery">Time of delivery (Day)</label>
                  <input type="number" name="time_delivery" value="" class="input-item time-delivery" min="0">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 category-area">
                <div class="input-group">
                  <label for="mjob_category">Category</label>
                  <select data-chosen-width="100%" data-chosen-disable-search="" data-placeholder="Choose categories"
                    name="mjob_category" id="mjob_category" class="chosen chosen-single tax-item required"
                    style="display: none;">
                    <option class=" copywriting cat-4 level-0" value="4">Copywriting</option>
                    <option class=" articles-blog-posts cat-34 level-1" value="34">Articles &amp; Blog Posts</option>
                    <option class=" business-copywriting cat-30 level-1" value="30">Business Copywriting</option>
                    <option class=" creative-writing cat-31 level-1" value="31">Creative Writing</option>
                    <option class=" press-releases cat-33 level-1" value="33">Press Releases</option>
                    <option class=" research-summaries cat-35 level-1" value="35">Research &amp; Summaries</option>
                    <option class=" resumes-cover-letters cat-32 level-1" value="32">Resumes &amp; Cover Letters
                    </option>
                    <option class=" digital-marketing cat-6 level-0" value="6">Digital Marketing</option>
                    <option class=" influencer-marketing cat-41 level-1" value="41">Influencer Marketing</option>
                    <option class=" mobile-advertising cat-42 level-1" value="42">Mobile Advertising</option>
                    <option class=" seo cat-43 level-1" value="43">SEO</option>
                    <option class=" social-media-marketing cat-40 level-1" value="40">Social Media Marketing</option>
                    <option class=" web-analytics cat-44 level-1" value="44">Web Analytics</option>
                    <option class=" graphics-design cat-3 level-0" value="3">Graphics &amp; Design</option>
                    <option class=" 2d-design-3d-modeling cat-24 level-1" value="24">2D Design &amp; 3D Modeling
                    </option>
                    <option class=" app-icon-design cat-26 level-1" value="26">App Icon Design</option>
                    <option class=" book-covers-packaging cat-23 level-1" value="23">Book Covers &amp; Packaging
                    </option>
                    <option class=" cartoons-caricatures cat-18 level-1" value="18">Cartoons &amp; Caricatures</option>
                    <option class=" customize-graphics cat-27 level-1" value="27">Customize Graphics</option>
                    <option class=" drawing-sketching cat-25 level-1" value="25">Drawing &amp; Sketching</option>
                    <option class=" flyers-posters cat-21 level-1" value="21">Flyers &amp; Posters</option>
                    <option class=" illustration cat-20 level-1" value="20">Illustration</option>
                    <option class=" photoshop-editing cat-19 level-1" value="19">Photoshop Editing</option>
                    <option class=" presentations-infographics cat-28 level-1" value="28">Presentations &amp;
                      Infographics</option>
                    <option class=" vector-tracing cat-29 level-1" value="29">Vector Tracing</option>
                    <option class=" web-mobile-design cat-22 level-1" value="22">Web &amp; Mobile Design</option>
                    <option class=" lifestyle cat-10 level-0" value="10">Lifestyle</option>
                    <option class=" animal-care-pets cat-64 level-1" value="64">Animal Care &amp; Pets</option>
                    <option class=" astrology-fortune-telling cat-60 level-1" value="60">Astrology &amp; Fortune Telling
                    </option>
                    <option class=" cooking-recipes cat-62 level-1" value="62">Cooking Recipes</option>
                    <option class=" health-fitness cat-59 level-1" value="59">Health &amp; Fitness</option>
                    <option class=" makeup-styling-beauty cat-63 level-1" value="63">Makeup, Styling &amp; Beauty
                    </option>
                    <option class=" relationship-advice cat-61 level-1" value="61">Relationship Advice</option>
                    <option class=" logo-design-branding cat-2 level-0" value="2">Logo Design</option>
                    <option class=" banner-ads cat-13 level-1" value="13">Banner Ads</option>
                    <option class=" business-cards-stationery cat-17 level-1" value="17">Business Cards &amp; Stationery
                    </option>
                    <option class=" invitations cat-16 level-1" value="16">Invitations</option>
                    <option class=" logo-customization cat-12 level-1" value="12">Logo Customization</option>
                    <option class=" logo-design cat-11 level-1" value="11">Logo Design</option>
                    <option class=" social-media-design cat-14 level-1" value="14">Social Media Design</option>
                    <option class=" t-shirt-design cat-15 level-1" value="15">T-Shirt Design</option>
                    <option class=" tech-support cat-9 level-0" value="9">Tech Support</option>
                    <option class=" convert-files cat-58 level-1" value="58">Convert Files</option>
                    <option class=" data-analysis-reports cat-57 level-1" value="57">Data Analysis &amp; Reports
                    </option>
                    <option class=" qa cat-56 level-1" value="56">QA</option>
                    <option class=" support-it cat-54 level-1" value="54">Support &amp; IT</option>
                    <option class=" user-testing cat-55 level-1" value="55">User Testing</option>
                    <option class=" translation cat-5 level-0" value="5">Translation</option>
                    <option class=" english-to-your-language cat-38 level-1" value="38">English to Your language
                    </option>
                    <option class=" proofreading-editing cat-37 level-1" value="37">Proofreading &amp; Editing</option>
                    <option class=" transcription cat-36 level-1" value="36">Transcription</option>
                    <option class=" your-language-to-english cat-39 level-1" value="39">Your language to English
                    </option>
                    <option class=" website-programming cat-8 level-0" value="8">Website &amp; Programming</option>
                    <option class=" mobile-apps-web cat-52 level-1" value="52">Mobile Apps &amp; Web</option>
                    <option class=" web-programming cat-51 level-1" value="51">Web Programming</option>
                    <option class=" website-builders-cms cat-53 level-1" value="53">Website Builders &amp; CMS</option>
                    <option class=" wordpress cat-7 level-0" value="7">WordPress</option>
                    <option class=" helpconsultation cat-50 level-1" value="50">Help/Consultation</option>
                    <option class=" psd-to-wordpress cat-48 level-1" value="48">PSD to WordPress</option>
                    <option class=" security cat-49 level-1" value="49">Security</option>
                    <option class=" wordpress-customization cat-47 level-1" value="47">WordPress Customization</option>
                    <option class=" wordpress-installation cat-45 level-1" value="45">WordPress Installation</option>
                    <option class=" wordpress-plug-ins cat-46 level-1" value="46">WordPress Plug-ins</option>
                  </select>
                  <div class="chosen-container chosen-container-single" style="width: 100%;" title=""
                    id="mjob_category_chosen"><a
                      class="chosen-single md-opjjpmhoiojifppkkcdabiobhakljdgm_doc"><span>Copywriting</span>
                      <div><b></b></div>
                    </a>
                    <div class="chosen-drop">
                      <div class="chosen-search"><input type="text" autocomplete="off"></div>
                      <ul class="chosen-results"></ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="mb-20">Description</label>
              <div id="wp-post_content-wrap" class="wp-core-ui wp-editor-wrap tmce-active">
                <link rel="stylesheet" id="dashicons-css"
                  href="https://microjobengine.enginethemes.com/microjobengine-diplomat/wp-includes/css/dashicons.min.css?ver=5.3.2"
                  type="text/css" media="all">
                <link rel="stylesheet" id="editor-buttons-css"
                  href="https://microjobengine.enginethemes.com/microjobengine-diplomat/wp-includes/css/editor.min.css?ver=5.3.2"
                  type="text/css" media="all">
                <div id="wp-post_content-editor-container" class="wp-editor-container">
                  <div id="mceu_7" class="mce-tinymce mce-container mce-panel" hidefocus="1" tabindex="-1"
                    role="application" style="visibility: hidden; border-width: 1px; width: 100%;">
                    <div id="mceu_7-body" class="mce-container-body mce-stack-layout">
                      <div id="mceu_8" class="mce-top-part mce-container mce-stack-layout-item mce-first">
                        <div id="mceu_8-body" class="mce-container-body">
                          <div id="mceu_9" class="mce-toolbar-grp mce-container mce-panel mce-first mce-last"
                            hidefocus="1" tabindex="-1" role="group">
                            <div id="mceu_9-body" class="mce-container-body mce-stack-layout">
                              <div id="mceu_10"
                                class="mce-container mce-toolbar mce-stack-layout-item mce-first mce-last"
                                role="toolbar">
                                <div id="mceu_10-body" class="mce-container-body mce-flow-layout">
                                  <div id="mceu_11"
                                    class="mce-container mce-flow-layout-item mce-first mce-last mce-btn-group"
                                    role="group">
                                    <div id="mceu_11-body">
                                      <div id="mceu_0" class="mce-widget mce-btn mce-first" tabindex="-1"
                                        aria-pressed="false" role="button" aria-label="Bold (⌘B)"><button
                                          id="mceu_0-button" role="presentation" type="button" tabindex="-1"><i
                                            class="mce-ico mce-i-bold"></i></button></div>
                                      <div id="mceu_1" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false"
                                        role="button" aria-label="Italic (⌘I)"><button id="mceu_1-button"
                                          role="presentation" type="button" tabindex="-1"><i
                                            class="mce-ico mce-i-italic"></i></button></div>
                                      <div id="mceu_2" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false"
                                        role="button" aria-label="Underline (⌘U)"><button id="mceu_2-button"
                                          role="presentation" type="button" tabindex="-1"><i
                                            class="mce-ico mce-i-underline"></i></button></div>
                                      <div id="mceu_3" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false"
                                        role="button" aria-label="Bulleted list (⌃⌥U)"><button id="mceu_3-button"
                                          role="presentation" type="button" tabindex="-1"><i
                                            class="mce-ico mce-i-bullist"></i></button></div>
                                      <div id="mceu_4" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false"
                                        role="button" aria-label="Numbered list (⌃⌥O)"><button id="mceu_4-button"
                                          role="presentation" type="button" tabindex="-1"><i
                                            class="mce-ico mce-i-numlist"></i></button></div>
                                      <div id="mceu_5" class="mce-widget mce-btn" tabindex="-1" aria-pressed="false"
                                        role="button" aria-label="Insert/edit link (⌘K)"><button id="mceu_5-button"
                                          role="presentation" type="button" tabindex="-1"><i
                                            class="mce-ico mce-i-link"></i></button></div>
                                      <div id="mceu_6" class="mce-widget mce-btn mce-last" tabindex="-1" role="button"
                                        aria-label="Remove link (⌃⌥S)"><button id="mceu_6-button" role="presentation"
                                          type="button" tabindex="-1"><i class="mce-ico mce-i-unlink"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="mceu_12" class="mce-edit-area mce-container mce-panel mce-stack-layout-item"
                        hidefocus="1" tabindex="-1" role="group" style="border-width: 1px 0px 0px;"><iframe
                          id="post_content_ifr" frameborder="0" allowtransparency="true"
                          title="Rich Text Area. Press Control-Option-H for help."
                          style="width: 100%; height: 250px; display: block;"></iframe></div>
                      <div id="mceu_13" class="mce-statusbar mce-container mce-panel mce-stack-layout-item mce-last"
                        hidefocus="1" tabindex="-1" role="group" style="border-width: 1px 0px 0px;">
                        <div id="mceu_13-body" class="mce-container-body mce-flow-layout">
                          <div id="mceu_14" class="mce-path mce-flow-layout-item mce-first">
                            <div class="mce-path-item">&nbsp;</div>
                          </div>
                          <div id="mceu_15" class="mce-flow-layout-item mce-last mce-resizehandle"><i
                              class="mce-ico mce-i-resize"></i></div>
                        </div>
                      </div>
                    </div>
                  </div><textarea class="wp-editor-area" rows="20" autocomplete="off" cols="40" name="post_content"
                    id="post_content" aria-hidden="true" style="display: none;"></textarea>
                </div>
              </div>

            </div>
            <div class="form-group group-attachment gallery_container" id="gallery_container">
              <label class="mb-20">Gallery</label>
              <div class="outer-carousel-gallery">
                <div class="img-avatar carousel-gallery">
                  <img width="100%"
                    src="https://microjobengine.enginethemes.com/microjobengine-diplomat/wp-content/themes/microjobengine/assets/img/image-avatar.jpg"
                    alt="">
                  <input type="hidden" class="input-item" name="et_carousels" value="">
                </div>
              </div>
              <div class="attachment-image has-image clearfix">
                <ul class="carousel-image-list image-list" id="image-list">

                </ul>

                <span class="image-upload carousel_container" id="carousel_container">
                  <span for="file-input" class="carousel_browse_button" id="carousel_browse_button">
                    <a class="add-img md-opjjpmhoiojifppkkcdabiobhakljdgm_doc"><img
                        src="https://microjobengine.enginethemes.com/microjobengine-diplomat/wp-content/themes/microjobengine/assets/img/icon-plus.png"
                        alt=""></a>
                  </span>
                </span>

                <span class="et_ajaxnonce" id="5776091294"></span>
              </div>
            </div>
            <div class="form-group clearfix">
              <label>Video</label>
              <input type="text" class="input-item form-control text-field" id="video_meta"
                placeholder="Add link from Youtube, Vimeo or .MP4 " name="video_meta" autocomplete="off"
                spellcheck="false">
              <ul class="skills-list" id="skills_list"></ul>
            </div>
            <div class="form-group clearfix">
              <label class="mb-20">Extra services</label>
              <div class="mjob-extras-wrapper">
              </div>
              <div class="add-more">
                <a href="#" class="mjob-add-extra-btn md-opjjpmhoiojifppkkcdabiobhakljdgm_doc">Add extra<span
                    class="icon-plus"><i class="fas fa-plus"></i></span></a>
              </div>
            </div>
            <div class="form-group clearfix skill-control">
              <label>Tags</label>
              <input type="text" class="form-control text-field skill" id="skill" placeholder="Enter microjob tags"
                name="" autocomplete="off" spellcheck="false">
              <ul class="skills-list" id="skills_list"></ul>
            </div>
            <script type="text/template" id="openingMessageTemplate">
              <div class="box-shadow opening-message">
                <div class="aside-title">
                    Opening Message <i class="fas fa-question-circle popover-opening-message" style="cursor: pointer" aria-hidden="true"></i>
                </div>
                <div class="content">
                    <div class="content-opening-message">
    
                    </div>
                    <a class="show-opening-message"></a>
                </div>
            </div>
        </script>
            <div class="form-group skill-control">
              <label for="opening_message">Opening message <i class="fas fa-question-circle popover-opening-message"
                  aria-hidden="true" data-original-title="" title=""></i></label>
              <p class="note-message">
                Opening message is automatically displayed as your first message in the order detail page. </p>
              <textarea name="opening_message" class="input-item"></textarea>
            </div>
            <div class="form-group">
              <button class="btn-save btn-diplomat" type="submit">SAVE</button>
              <a href="=&quot;#&quot;"
                class="btn-discard mjob-discard-action md-opjjpmhoiojifppkkcdabiobhakljdgm_doc">DISCARD</a>
              <input type="hidden" class="input-item post-service_nonce" name="_wpnonce" value="143b86419e">
            </div>
          </form>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="mjob-single-aside">
          <div class="box-shadow box-aside-stat">

            <div class="mjob-single-stat">
              <div class="stat-block clearfix">
                <div class="vote pull-left">
                  <div class="rate-it" data-score="3" title="nice"><i data-alt="1" class="fas fa-star star-on-png"
                      title="nice"></i>&nbsp;<i data-alt="2" class="fas fa-star star-on-png" title="nice"></i>&nbsp;<i
                      data-alt="3" class="fas fa-star star-on-png" title="nice"></i>&nbsp;<i data-alt="4"
                      class="fas fa-star-o off star-off-png" title="nice"></i>&nbsp;<i data-alt="5"
                      class="fas fa-star-o off star-off-png" title="nice"></i><input name="score" type="hidden"
                      value="3" readonly=""></div>
                  <span class="total-review">(1)</span>
                </div>
                <span class="price pull-right"><span title="10 TND"><span
                      class="mje-price">10<sup>TND</sup></span></span></span>
              </div>

              <div class="stat-block">
                <ul>
                  <li class="clearfix">
                    <span class="pull-left"><i class="fas fa-star"></i>Note Moyenne</span>
                    <div class="total-number pull-right">3</div>
                  </li>
                  <li class="clearfix">
                    <span class="pull-left"><i class="fas fa-commenting"></i>Commentaires</span>
                    <div class="total-number pull-right">1</div>
                  </li>
                  <li class="clearfix">
                    <span class="pull-left"><i class="fas fa-shopping-cart"></i>Nombre de ventes</span>
                    <div class="total-number pull-right">0</div>
                  </li>
                  <li class="clearfix">
                    <span class="pull-left"><i class="fas fa-calendar"></i>Délais de livraison</span>
                  <div class="total-number time-delivery-label pull-right">{{$service->max_days_to_complete}} (jours)</div>
                  </li>
                  <li class="clearfix">
                    <span class="pull-left"><i class="fas fa-calendar"></i>Révisions/Retouches</span>
                  <div class="total-number time-delivery-label pull-right">{{$service->max_revisions}}</div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="action">
              <button class="btn-order waves-effect waves-light btn-diplomat mjob-order-action">
                Commander (<span class="mjob-price mje-price-text"><span title="10"><span
                      class="mje-price">10<sup>TND</sup></span></span></span>) </button>
            </div>
            <!--
            <div class="add-extra mjob-add-extra">
              <span class="extra">EXTRA</span>
              <div class="extra-container">
                <ul class="list-extra mjob-list-extras"></ul>
                <p class="no-extra">There are no extra services</p>
                <script type="data/json" class="extra_postdata">[]</script>
              </div>
            </div>
         
            <div class="custom-order-link">
              <div>
                <a id="bt-send-custom" class="bt-send-custom md-opjjpmhoiojifppkkcdabiobhakljdgm_doc"
                  data-mjob-name="I will create the best DJ logo on MicrojobEngine" data-mjob="425"
                  data-conversation-guid="" data-conversation-parent="0" data-to-user="7" data-from-user="0"
                  style="cursor: pointer">Send custom order<i class="fas fa-paper-plane"></i></a>
              </div>
            </div>
             -->
          </div>
          <div class="box-aside box-shadow">
            <div class="personal-profile">
              <div class="float-center">
                <img src="{{empty($user->avatar) ? '/assets/images/default/avatar.png' : $user->avatar}}" class="avatar" alt="{{$user->name}}">
              </div>
            <h4 class="float-center">{{$user->name}}</h4>
              <div class="line">
                <span class="line-distance"></span>
              </div>
              <ul class="profile">
                <li class="location clearfix">
                  <div class="pull-left">
                    <span><i class="fas fa-map-marker"></i>From </span>
                  </div>
                  <div class="pull-right">
                    Vietnam </div>
                </li>

                <li class="language clearfix">
                  <div class="pull-left">
                    <span><i class="fas fa-globe"></i>Languages </span>
                  </div>
                  <div class="pull-right">
                    <p class="lang-item">English</p>
                    <p class="lang-item">Vietnamese</p>
                  </div>
                </li>


                <li class="bio clearfix">
                  <span> <i class="fas fa-info-circle"></i>Biographie</span>
                  <div class="content-bio">
N/A
                  </div>
                </li>

              </ul>


              <div class="link-personal">
                <ul>
                  <li><a href="#" class="contact-link do-contact" data-touser="7">Contacter le vendeur<i class="fas fa-comment"></i></a></li>
                  <li><a href="#" class="profile-link">Voir le profil<i class="fas fa-user"></i></a></li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection

{{-- 
@section('content')
  <div class="widget__title card__header card__header--has-btn">
     <div class="widget_title1">
        <h4>All Services</h4>
     </div>
  </div>
  <br><br>

  <div class="list-group">
    @foreach ($services as $service)
      <div class="media single-service" style="padding:0px 40px;">
        <div class="service-image-container">
          <a href="#">
            <img class="media-object" src="{{asset('_assets/users/service_images/'.$service->serviceImages()->first()->image_name)}}"
alt="...">
@if ($service->feature == 1)
<h5><span style="display:block;" class="label label-primary">Featured</span></h5>
@endif
</a>
</div>
<div class="media-body">
  <div class="media-heading-price-container">
    <h2 class="media-heading"><a href="{{route('services.show', [$service->id, $service->user->id])}}"
        class="text-primary">{!!(strlen($service->service_title)>40) ? substr($service->service_title, 0, 40) .
        '...' : $service->service_title!!}</a></h2>
    <small class="pull-right text-danger"><strong>{{$service->price}} {{$gs->base_curr_symbol}}</strong></small>
  </div>
  <p style="clear:both;"></p>
  <div class="col-md-12 description-username-container">
    <p class="service-description col-md-10">
      {!!(strlen(strip_tags($service->description))>120) ? substr(strip_tags($service->description), 0, 120) .
      '...' : strip_tags($service->description)!!}
    </p>
    <small class="col-md-2 username"><strong><a class="text-danger"
          href="{{route('users.profile', $service->user->id)}}">{{$service->user->username}}</a></strong></small>
  </div>
  <div class="row">
    <div class="col-md-6">
      <button style="margin-right:10px;" class="btn btn-sm btn-primary pull-left" type="button" name="button"><i
          class="fas fa-thumbs-o-up" aria-hidden="true"></i>
        {{count($service->orders()->where('like', 1)->get())}}</button>
      <button class="btn btn-sm btn-warning pull-left" type="button" name="button"><i class="fas fa-thumbs-o-down"
          aria-hidden="true"></i> {{count($service->orders()->where('like', 0)->get())}}</button>
    </div>
    <div class="col-md-6">
      <button class="btn btn-danger btn-sm pull-right" class="pull-right" onclick="placeOrder({{$service->id}})">
        <i class="fas fa-shopping-cart" aria-hidden="true"></i>
        Order Now
      </button>
    </div>
  </div>
</div>
@if (!$loop->last)
<hr>
@endif
</div>
@endforeach
<div class="row">
  <div class="text-center">
    {{$services->links()}}
  </div>
</div>
</div>

@component('users.components.balanceShortage')
@endcomponent
@component('users.components.success')
@endcomponent
@endsection
--}}



@push('scripts')
@auth
<script>
  function placeOrder(serviceID) {

          var fd = new FormData();
          fd.append('serviceID', serviceID);
          $.ajaxSetup({
              headers: {
                  'X-CSRF-Token': $('meta[name=_token]').attr('content')
              }
          });
          // console.log(token + ' ' + serviceID);
          var c = confirm('Are you sure you want to place this order?');

          if(c == true) {
            $.ajax({
              url: '{{route('buyer.placeOrder')}}',
              type: 'POST',
              data: fd,
              contentType: false,
              processData: false,
              success: function(data) {
                // if user balance runs into shortage...
                console.log(data);
                if(typeof data.balance != 'undefined') {
                  var snackbar = document.getElementById('snackbar');
                  snackbar.innerHTML = "You don't have enough balance to buy this Gig!";
                  snackbar.className = "show";
                  setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
                }
                 // if order is placed successfully...
                else {
                  var url = "{{route('buyer.contactOrder')}}/" + data;
                  console.log(url);
                  var snackbarSuccess = document.getElementById('snackbarSuccess');
                  snackbarSuccess.innerHTML = "Order has been placed successfully!";
                  snackbarSuccess.className = "show";
                  setTimeout(function() {
                    snackbarSuccess.className = snackbarSuccess.className.replace("show", "");
                  }, 3000);
                  window.location.href = url;
                }
              }
            });
          }

        }
</script>
@endauth
@guest
<script>
  function placeOrder(serviceID) {
        window.location = '{{route('login')}}';
      }
</script>
@endguest
@endpush