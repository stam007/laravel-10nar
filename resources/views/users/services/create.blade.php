@extends('layouts.app')

@section('title', 'Ajouter un micro-service | '.$gs->website_title)


{{-- All necessary JS for NIC Editor --}}
@push('nic-editor-scripts')
<script src="{{asset('assets/admin/js/nic-edit/nicEdit.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
          new nicEditor({iconsPath : "{{asset('assets/admin/js/nic-edit/nicEditorIcons.gif')}}"}).panelInstance('description');
    });
</script>
@endpush

@push('styles')
<style media="screen">
  .nicEdit-main {
    background-color: white;
    resize: vertical;
    margin: 0px !important;
  }

  input:not(.updateBtn) {
    margin: 0px !important;
    background-color: white !important;
  }

  .login-admin {
    padding: 0px 100px;
  }

  @media screen and (max-width: 500px) {
    .login-admin {
      padding: 0px;
    }
  }
</style>
@endpush

@section('content')
<div id="content" class="mjob-single-page">
  <div class="container mjob-single-primary">
    <div class="row">
      <div class="col-md-12">
        <div class="mjob-single-description mjob-single-block pad-lr-30">
          @if (!$errors->isEmpty())
          <div class="alert alert_danger alert_sm" style="animation-delay: .1s">
            <div class="alert--icon">
              <i class="fas fa-times-circle"></i>
            </div>
            <div class="alert--content">
              @foreach ($errors->all() as $error)
              <p>{{$error}}</p>
              @endforeach
            </div>
            <div class="alert--close">
              <i class="far fa-times-circle"></i>
            </div>
          </div>
          @endif
          <div class="mjob-edit-content">
            <form onsubmit="storeService(event)" class="post-job step-post post et-form edit-mjob-form"
              id="storeServiceForm" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <p class="mjob-title">Ajouter un micro-service</p>
              <div id="success-msg-box" style='display:none'>
                <i class="far fa-check-circle"></i>                
                <span id='success-msg-text'></span>
              </div>       
              <div class="form-group clearfix">
                <div class="input-group">
                  <label for="post_title" class="input-label">Titre *</label>
                  <div style="display:flex;align-items:stretch;">
                    <div style="padding-top: 12px; width: 84px; border-bottom: 1px solid rgba(137, 138, 144, 0.2);font-size: 16px;">
                      Je vais
                    </div>
                    <div style="width: 100%;">
                      <input type="text" class="input-item" id="serviceTitle" name="serviceTitle" maxlength="200">
                    </div>
                    <div style="padding-top: 12px;width: 220px;padding-left: 10px;border-bottom: 1px solid rgba(137, 138, 144, 0.2);font-size: 16px;">
                      pour 10 dinars.
                    </div>
                  </div>
                  <span class="c-validation"></span>
                </div>
              </div>

              <div class="form-group">
                <div class="input-group">
                  <label for="description" class="input-label">Description *</label>
                  <textarea id="description" name="description" rows="10" cols="80"></textarea>
                  <span class="c-validation"></span>
                </div>
              </div>

              <div class="form-group row clearfix">

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="input-group">
                    <label for="category">Catégorie *</label>
                    <select id="category" name="category" class="dropdown-custom dropdown-btn">
                      @foreach ($categories as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    </select>
                    <span class="c-validation"></span>
                  </div>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="input-group">
                    <label for="subcategory">Sous Catégorie *</label>
                    <select id="subcategory" name="subcategory" class="dropdown-custom dropdown-btn">
                    </select>
                    <span class="c-validation"></span>
                  </div>
                </div>

              </div>

              <div class="form-group row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="input-group delivery-time">
                    <label for="maxDaysToComplete">Délais de livraison (en Jours) *</label>
                    <select id="maxDaysToComplete" name="maxDaysToComplete" class="dropdown-custom dropdown-btn">
                      <?php $count=30 ?>
                      @for ($i=1; $i<=30; $i++) <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                    <span class="c-validation"></span>
                  </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="input-group">
                    <label for="maxRevisions">Révisions/Retouches *</label>
                    <select id="maxRevisions" name="maxRevisions" class="dropdown-custom dropdown-btn">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="Illimitée">Illimitée</option>
                    </select>
                    <span class="c-validation"></span>
                  </div>
                </div>
              </div>

              <div class="form-group row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 category-area">
                  <div class="input-group">
                    <label for="mjob_category" class="title-M hint--right hint--large"
                      aria-label="Maximum 3 photos aux formats: .png, .jpg ou .jpeg. Les images chargées seront redimensionnées en 760X400.">
                      Images *&nbsp;<i class="fas fa-info-circle"></i></label>
                    <div class="" id="serviceImagesDiv">
                      <img class="serviceImages-cont" width="100" src="" alt="">
                      <img class="serviceImages-cont" width="100" src="" alt="">
                      <img class="serviceImages-cont" width="100" src="" alt="">
                    </div>
                    <label class="btn-order waves-effect waves-light btn-diplomat mjob-order-action"
                      style="color:white;">
                      <input id="serviceImages" name="images[]" style="display:none;" type="file" multiple /><i
                        class="far fa-images"></i>Ajouter les images
                    </label>
                    <p class="note-message">Appuyez sur la touche <strong>Ctrl</strong> et maintenez la touche enfoncée
                      pour pouvoir selectionner plusieurs photos.</p>
                    <span class="c-validation"></span>
                    <span class="c-validation"></span>
                    <span class="c-validation"></span>
                  </div>
                </div>
              </div>

              <div class="form-group row clearfix">
                <label>Ajouter un lien YouTube</label>
                <input type="text" class="input-item form-control text-field" id="videoURL"
                  placeholder="https://www.youtube.com/watch?v=XXXXX..." name="videoURL" autocomplete="off" spellcheck="false">
              </div>

              <div class="form-group row clearfix">
                <label class="mb-20">Options supplémentaires *</label>
                <div class="add-more">
                  <a href="#" class="mjob-add-extra-btn">Ajouter<span class="icon-plus"><i
                        class="fas fa-plus"></i></span></a>
                </div>
                <span class="c-validation"></span>
              </div>

              <div class="form-group row clearfix">
                <label>Tags *</label>
                <input id="tagInput" onkeypress="addTags(event,this.value)" type="text"
                  class="form-control text-field skill" placeholder="Tags" value="" autocomplete="off"
                  spellcheck="false">
                <p class="note-message">
                  Ajouter 5 tags au maximum.
                </p>
                <div class="tags">
                  <ul id="tags">
                  </ul>
                </div>
                <span class="c-validation"></span>
              </div>

              <div class="form-group" style="text-align:center;">
                <a id="cancelBtn" class="btn-discard mjob-discard-action" href="#"
                  onclick="window.history.back();return false;">Annuler</a>
                <button id="createBtn" class="btn-order waves-effect waves-light btn-diplomat mjob-order-action"
                  type="submit"><i class="far fa-save"></i>&nbsp;Ajouter</button>
              </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
  $(document).ready(function() {
  
    var categoryId = $('#category').val();
      $.get('/ajax-get-subcategories?categoryId='+categoryId, function(data) {
        $('#subcategory').empty();
$.each(data, function(index, subcategory){
  $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.name+'</option>')

})      });
    $("#category").on("change", function() {
      var categoryId = $(this).val();
      $.get('/ajax-get-subcategories?categoryId='+categoryId, function(data) {
        $('#subcategory').empty();
$.each(data, function(index, subcategory){
  $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.name+'</option>')

})      });

    });

});

  var tagsArr = [];

      // adding tags to UI dynamically and adding tags to 'tagsArr'...
      function addTags(e, tag) {
        if(e.keyCode == 13) {
          e.preventDefault();
          if(e.target.value.length != 0 && tagsArr.length != 5) {
            // =======Adding tags dynamically ========== //
              var tags = document.getElementById('tags');
              var li = document.createElement("li");
              li.setAttribute('class', 'skill-name-profile');

              //var tagText = document.createTextNode(tag);
              var span = document.createElement("span");
              span.textContent = tag;
              //span.setAttribute('class', 'skill-name-profile');
              li.appendChild(span);
              tags.appendChild(li);
              // adding close icon to button...
              var closeIcon = document.createElement('i');
              closeIcon.setAttribute('class', 'far fa-trash-alt');
              closeIcon.setAttribute('style', 'margin-left:3px');
              closeIcon.setAttribute('onclick', 'removeTag(event)');
              span.appendChild(closeIcon);
              tags.appendChild(li);


/*
            var tags = document.getElementById('tags');

            // creating button...
            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('class', 'btn btn-primary');
            button.setAttribute('style', 'margin-right:5px');

            // adding button text to button...
            var buttonText = document.createTextNode(tag);
            button.appendChild(buttonText);

            // adding close icon to button...
            var closeIcon = document.createElement('i');
            closeIcon.setAttribute('class', 'fas fa-close');
            closeIcon.setAttribute('style', 'margin-left:3px');
            closeIcon.setAttribute('onclick', 'removeTag(event)');
            button.appendChild(closeIcon);

            // adding buttons under 'tags' div...
            tags.appendChild(button);*/
            // =======Adding tags dynamically ========== //
            document.getElementById('tagInput').value = '';
            console.log('Enter pressed');
            tagsArr.push(tag);
            console.log(tagsArr);
          }
        }

      }

      // Removing tags from UI dynamically and removing from tagsArr...
      function removeTag(e) {
        for (var i = 0; i < tagsArr.length; i++) {
          if (tagsArr[i] == e.target.parentElement.innerText) {
              tagsArr.splice(i, 1);
              var elt = e.target.parentElement;
              elt.parentElement.remove();
              
              //e.target.closest('li.span').remove();
              //this.parentNode.remove();

              console.log(tagsArr);
              break;
          }
        }
      }

      // storing service to database...
      function storeService(e) {
        e.preventDefault();
        var storeServiceForm = document.getElementById('storeServiceForm');
        // getting description content...
        var descriptionElement = new nicEditors.findEditor('description');
        description = descriptionElement.getContent();
        // getting Introductions to buyer content...
        /*
        var introToBuyerElement = new nicEditors.findEditor('introToBuyer');
        introToBuyer = introToBuyerElement.getContent();*/
        var fd = new FormData(storeServiceForm);
        fd.append('tags', JSON.stringify(tagsArr));
        fd.append('description', description);
        //fd.append('introToBuyer', introToBuyer);
        // console.log(tagsArr);
        document.getElementById('createBtn').innerHTML = '<i class="fas fa-refresh fa-spin" style="font-size:24px"></i>';
        $.ajax({
          url: '{{route('services.store')}}',
          type: 'POST',
          data: fd,
          contentType: false,
          processData: false,
          success: function(data) {
            document.getElementById('createBtn').innerHTML = '<i class="far fa-save"></i>&nbsp;Ajouter';
            console.log(data);

            var em = document.getElementsByClassName("c-validation");
            // after returning from the controller we are clearing the
            // previous error messages...
            for(i=0; i<em.length; i++) {
              em[i].innerHTML = '';
            }
            if(data && data.type == 'success') {
              document.getElementById('storeServiceForm').reset();
              // making NIC edit textarea null after submitting form successfully...
              descriptionElement.setContent('');
              document.getElementById('tags').innerHTML = '';
              document.getElementById('serviceImagesDiv').innerHTML = '';

              tagsArr = [];
              var successMsgText= document.getElementById('success-msg-text');
              successMsgText.innerHTML = "Félicitation! Votre micro-service a été rajouté avec succès.<br/> Vous pouvez le consulter sur: <a href='"+data.url+"'>"+data.url+"</a>";
              document.getElementById('success-msg-box').style.display = 'block';
              $("html, body").animate({ scrollTop: 0 }, "slow");
              $("#createBtn").attr('disabled', true);
              $("#createBtn").hide();
              $("#cancelBtn").attr('disabled', true);
              $("#cancelBtn").hide();
            }

            // Showing error messages in the HTML...
            if(typeof data.error != 'undefined') {
              if(typeof data.serviceTitle != 'undefined') {
                em[0].innerHTML = data.serviceTitle[0];
              }
              if(typeof data.description != 'undefined') {
                em[1].innerHTML = data.description[0];
              }
              if(typeof data.category != 'undefined') {
                em[2].innerHTML = data.category[0];
              }
              if(typeof data.subcategory != 'undefined') {
                em[3].innerHTML = data.subcategory[0];
              }
              if(typeof data.maxDaysToComplete != 'undefined') {
                em[4].innerHTML = data.maxDaysToComplete[0];
              }
              if(typeof data.maxRevisions != 'undefined') {
                em[5].innerHTML = data.maxRevisions[0];
              }
              if (typeof data.files != 'undefined') {
                em[6].innerHTML = data.files[0];
              }
              if (typeof data.fileCount != 'undefined') {
                em[7].innerHTML = data.fileCount[0];
              }
              if (typeof data.images != 'undefined') {
                em[8].innerHTML = data.images[0];
              }
              if(typeof data.videoURL != 'undefined') {
                em[9].innerHTML = data.videoURL[0];
              }
              if(typeof data.tags != 'undefined') {
                em[10].innerHTML = data.tags[0];
              }
            }
          }
        });
      }
</script>

{{-- Image show after selecting --}}
<script>
  $(document).on('change', '#serviceImages', function(e) {
      var files = this.files;
      console.log(files);
      if (this.files.length) {
        for (let i = 0; i < this.files.length; i++) {
            var file = this.files[i];
            var reader = new FileReader();

            reader.onload = function(e) {
                var data = e.target.result;

                document.getElementsByClassName('serviceImages-cont')[i].setAttribute('src', data);
            };

            reader.readAsDataURL(file);
        }
      }

  });


</script>

@endpush