@extends('layouts.app')

@section('meta-ajax')
<meta name="_token" content="{{ csrf_token() }}" />
@endsection

@section('title', $gs->website_title .' | La première platefome de mico-services en Tunisie.')

@section('slider')
@include('users.home.slider')
@endsection

@section('top-static-content')
@include('users.home.static_content_1')
@endsection

@section('content')
<!--SECTION PREMIUM MICROSERVICES-->
<div class="block-items block-items-diplomat">
    <div class="container">
        <h6 class='text-uppercase'>Microservices de vendeurs vérifiés <br />
            <small class='text-muted' style='font-size:50%;'>Confiez votre demande à nos vendeurs vérifiés
            </small>
        </h6>
        <ul class="row mjob-list">
@foreach ($data['services'] as $service)
<li class="col-lg-3 col-md-3 col-sm-6 col-mobile-12">
    <div class="mjob-item">
        <div class="mjob-item__image">
        <a href="{{"micro-service/{$service->id}/{$service->slug}"}}" class="">
                <img src="{{asset('uploads/microservices/' . $service->serviceImages->first()->image_name)}}" alt="">
            </a>
        </div><!-- end .mjob-item__image -->

        <div class="mjob-item__entry">
            <div class="mjob-item__title">
                <h2 class="trimmed" title="I can design a professional business card">
                    <div class="dotdotdot" style="height: auto; width: auto;">
                        <a href="{{"micro-service/{$service->id}/{$service->slug}"}}">Je vais {{$service->service_title}} pour 10 dinars.</a>
                    </div>
                </h2>
            </div><!-- end .mjob-item__title -->
            <div class=" mjob-item__author">
                <a class="serviceAuthor" href="#">
                    <div class="sA-avatar">
                        <img class='img-circle' src="{{empty($service->user->avatar) ? '/assets/images/default/avatar.png' : $service->user->avatar}}" width="42px" height="auto"/>
                    </div>
                    <div class="minWidth0">
                        <div class="sA-name">
                            <span>par</span>&nbsp;
                            <em class="sA-name__nd flexTextOverflowEllipsis">
                                {{ $service->user->name}}
                            </em>
                            <span class="badgeVerified can_is_verified">
                                <i class="fas fa-check-circle" aria-hidden="true"></i>
                                <!-- <span>Vérifié</span> -->
                            </span>
                        </div>
                        <div class="displayFlex alignItemsCenter">
                            <div class="sA-stars stars">
                                <span class="star">
                                    <svg width="15" height="14" aria-hidden="true">
                                        <use class="star__full" xlink:href="#svg-star-full">
                                            <symbol viewBox="0 0 15 14" xmlns="http://www.w3.org/2000/svg"
                                                id="svg-star-full">
                                                <path fill="var(--main_color_link)"
                                                    d="M14.757 6.095l-3.044 2.896.718 4.09a.779.779 0 0 1-.321.768.822.822 0 0 1-.848.06l-3.763-1.931-3.763 1.931a.824.824 0 0 1-.848-.059.777.777 0 0 1-.32-.768l.719-4.09L.242 6.095a.772.772 0 0 1-.204-.805.8.8 0 0 1 .65-.534l4.207-.597L6.778.438A.804.804 0 0 1 7.499 0c.307 0 .586.17.721.438l1.883 3.721 4.208.597c.303.043.555.25.648.534a.77.77 0 0 1-.203.805z">
                                                </path>
                                            </symbol>
                                        </use>
                                    </svg>
                                </span>
                                <span class="star">
                                    <svg width="15" height="14" aria-hidden="true">
                                        <use class="star__full" xlink:href="#svg-star-full"></use>
                                    </svg>
                                </span>
                                <span class="star">
                                    <svg width="15" height="14" aria-hidden="true">
                                        <use class="star__full" xlink:href="#svg-star-full"></use>
                                    </svg>
                                </span>
                                <span class="star">
                                    <svg width="15" height="14" aria-hidden="true">
                                        <use class="star__full" xlink:href="#svg-star-full"></use>
                                    </svg>
                                </span>
                                <span class="star">
                                    <svg width="15" height="14" aria-hidden="true">
                                        <use class="star__full" xlink:href="#svg-star-full"></use>
                                    </svg>
                                </span>

                            </div>
                            <div class="countComments" title="5 avis">(5)</div>
                        </div>
                    </div>
                </a>
            </div><!-- end .mjob-item__author -->

            <div class="mjob-item__price">
                <div class="mjob-item__price-inner">
                    <span class="starting-text"><i class="fas fa-eye" aria-hidden="true"></i>1</span>
                    <span class="price-text customize-color"><span title="10 DT"><span
                                class="mje-price">10</span><sup>DT</sup></span></span>
                </div>
            </div><!-- end .mjob-item__price -->

            <div class="mjob-item__bottom clearfix">
                <div class="text-center">
                    <a href="{{"micro-service/{$service->id}/{$service->slug}"}}">
                        <div class="btn btn-greyLight" tabindex="-1" aria-hidden="true">Commander
                        </div>
                    </a>
                </div>
            </div><!-- end .mjob-item__bottom -->

        </div><!-- end .mjob-item__entry -->
    </div><!-- end .mjob-item -->
</li>
@endforeach
        </ul>
        <div class="view-all-jobs-wrap">
            <a class="btn-diplomat waves-effect waves-light" href="#"> Voir tout</a>
        </div>
    </div>
</div>
@include('users.home.static_content_2')
<!--SECTION LAST MICROSERVICES-->
<div class="block-items block-items-diplomat">
    <div class="container">
        <h6 class='text-uppercase'>Microservices ajoutés récemment<br />
            <small class='text-muted' style='font-size:50%;'>Profitez de nos dernières offres
            </small>
        </h6>
        <ul class="row mjob-list">
            @foreach ($data['services'] as $service)
            <li class="col-lg-3 col-md-3 col-sm-6 col-mobile-12">
                <div class="mjob-item">
                    <div class="mjob-item__image">
                    <a href="{{"micro-service/{$service->id}/{$service->slug}"}}" class="">
                            <img src="{{asset('uploads/microservices/' . $service->serviceImages->first()->image_name)}}" alt="">
                        </a>
                    </div><!-- end .mjob-item__image -->
            
                    <div class="mjob-item__entry">
                        <div class="mjob-item__title">
                            <h2 class="trimmed" title="I can design a professional business card">
                                <div class="dotdotdot" style="height: auto; width: auto;">
                                    <a href="{{"micro-service/{$service->id}/{$service->slug}"}}">Je vais {{$service->service_title}} pour 10 dinars.</a>
                                </div>
                            </h2>
                        </div><!-- end .mjob-item__title -->
                        <div class=" mjob-item__author">
                            <a class="serviceAuthor" href="#">
                                <div class="sA-avatar">
                                    <img class='img-circle' src="{{empty($service->user->avatar) ? '/assets/images/default/avatar.png' : $service->user->avatar}}" width="42px" height="auto"/>
                                </div>
                                <div class="minWidth0">
                                    <div class="sA-name">
                                        <span>par</span>&nbsp;
                                        <em class="sA-name__nd flexTextOverflowEllipsis">
                                            {{ $service->user->name}}
                                        </em>
                                        <span class="badgeVerified can_is_verified">
                                            <i class="fas fa-check-circle" aria-hidden="true"></i>
                                            <!-- <span>Vérifié</span> -->
                                        </span>
                                    </div>
                                    <div class="displayFlex alignItemsCenter">
                                        <div class="sA-stars stars">
                                            <span class="star">
                                                <svg width="15" height="14" aria-hidden="true">
                                                    <use class="star__full" xlink:href="#svg-star-full">
                                                        <symbol viewBox="0 0 15 14" xmlns="http://www.w3.org/2000/svg"
                                                            id="svg-star-full">
                                                            <path fill="var(--main_color_link)"
                                                                d="M14.757 6.095l-3.044 2.896.718 4.09a.779.779 0 0 1-.321.768.822.822 0 0 1-.848.06l-3.763-1.931-3.763 1.931a.824.824 0 0 1-.848-.059.777.777 0 0 1-.32-.768l.719-4.09L.242 6.095a.772.772 0 0 1-.204-.805.8.8 0 0 1 .65-.534l4.207-.597L6.778.438A.804.804 0 0 1 7.499 0c.307 0 .586.17.721.438l1.883 3.721 4.208.597c.303.043.555.25.648.534a.77.77 0 0 1-.203.805z">
                                                            </path>
                                                        </symbol>
                                                    </use>
                                                </svg>
                                            </span>
                                            <span class="star">
                                                <svg width="15" height="14" aria-hidden="true">
                                                    <use class="star__full" xlink:href="#svg-star-full"></use>
                                                </svg>
                                            </span>
                                            <span class="star">
                                                <svg width="15" height="14" aria-hidden="true">
                                                    <use class="star__full" xlink:href="#svg-star-full"></use>
                                                </svg>
                                            </span>
                                            <span class="star">
                                                <svg width="15" height="14" aria-hidden="true">
                                                    <use class="star__full" xlink:href="#svg-star-full"></use>
                                                </svg>
                                            </span>
                                            <span class="star">
                                                <svg width="15" height="14" aria-hidden="true">
                                                    <use class="star__full" xlink:href="#svg-star-full"></use>
                                                </svg>
                                            </span>
            
                                        </div>
                                        <div class="countComments" title="5 avis">(5)</div>
                                    </div>
                                </div>
                            </a>
                        </div><!-- end .mjob-item__author -->
            
                        <div class="mjob-item__price">
                            <div class="mjob-item__price-inner">
                                <span class="starting-text"><i class="fas fa-eye" aria-hidden="true"></i>1</span>
                                <span class="price-text customize-color"><span title="10 DT"><span
                                            class="mje-price">10</span><sup>DT</sup></span></span>
                            </div>
                        </div><!-- end .mjob-item__price -->
            
                        <div class="mjob-item__bottom clearfix">
                            <div class="text-center">
                                <a href="{{"micro-service/{$service->id}/{$service->slug}"}}">
                                    <div class="btn btn-greyLight" tabindex="-1" aria-hidden="true">Commander
                                    </div>
                                </a>
                            </div>
                        </div><!-- end .mjob-item__bottom -->
            
                    </div><!-- end .mjob-item__entry -->
                </div><!-- end .mjob-item -->
            </li>
            @endforeach
        </ul>

        <div class="view-all-jobs-wrap">
            <a class="btn-diplomat waves-effect waves-light"
                href="https://microjobengine.enginethemes.com/microjobengine-diplomat/blog/mjob_post/">
                Voir tout</a>
        </div>
    </div>
</div>
@include('users.home.static_content_3')
@endsection

{{-- 
@section('content')
  <div class="widget__title card__header card__header--has-btn">
     <div class="widget_title1">
        <h4>All Services</h4>
     </div>
  </div>
  <br><br>

  <div class="list-group">
    @foreach ($services as $service)
      <div class="media single-service" style="padding:0px 40px;">
        <div class="service-image-container">
          <a href="#">
            <img class="media-object" src="{{asset('_assets/users/service_images/'.$service->serviceImages()->first()->image_name)}}"
alt="...">
@if ($service->feature == 1)
<h5><span style="display:block;" class="label label-primary">Featured</span></h5>
@endif
</a>
</div>
<div class="media-body">
    <div class="media-heading-price-container">
        <h2 class="media-heading"><a href="{{route('services.show', [$service->id, $service->user->id])}}"
                class="text-primary">{!!(strlen($service->service_title)>40) ? substr($service->service_title, 0, 40) .
                '...' : $service->service_title!!}</a></h2>
        <small class="pull-right text-danger"><strong>{{$service->price}} {{$gs->base_curr_symbol}}</strong></small>
    </div>
    <p style="clear:both;"></p>
    <div class="col-md-12 description-username-container">
        <p class="service-description col-md-10">
            {!!(strlen(strip_tags($service->description))>120) ? substr(strip_tags($service->description), 0, 120) .
            '...' : strip_tags($service->description)!!}
        </p>
        <small class="col-md-2 username"><strong><a class="text-danger"
                    href="{{route('users.profile', $service->user->id)}}">{{$service->user->username}}</a></strong></small>
    </div>
    <div class="row">
        <div class="col-md-6">
            <button style="margin-right:10px;" class="btn btn-sm btn-primary pull-left" type="button" name="button"><i
                    class="fas fa-thumbs-o-up" aria-hidden="true"></i>
                {{count($service->orders()->where('like', 1)->get())}}</button>
            <button class="btn btn-sm btn-warning pull-left" type="button" name="button"><i class="fas fa-thumbs-o-down"
                    aria-hidden="true"></i> {{count($service->orders()->where('like', 0)->get())}}</button>
        </div>
        <div class="col-md-6">
            <button class="btn btn-danger btn-sm pull-right" class="pull-right" onclick="placeOrder({{$service->id}})">
                <i class="fas fa-shopping-cart" aria-hidden="true"></i>
                Order Now
            </button>
        </div>
    </div>
</div>
@if (!$loop->last)
<hr>
@endif
</div>
@endforeach
<div class="row">
    <div class="text-center">
        {{$services->links()}}
    </div>
</div>
</div>

@component('users.components.balanceShortage')
@endcomponent
@component('users.components.success')
@endcomponent
@endsection
--}}



@push('scripts')
@auth
<script>
    /*
    function placeOrder(serviceID) {

          var fd = new FormData();
          fd.append('serviceID', serviceID);
          $.ajaxSetup({
              headers: {
                  'X-CSRF-Token': $('meta[name=_token]').attr('content')
              }
          });
          // console.log(token + ' ' + serviceID);
          var c = confirm('Are you sure you want to place this order?');

          if(c == true) {
            $.ajax({
              url: '{{route('buyer.placeOrder')}}',
              type: 'POST',
              data: fd,
              contentType: false,
              processData: false,
              success: function(data) {
                // if user balance runs into shortage...
                console.log(data);
                if(typeof data.balance != 'undefined') {
                  var snackbar = document.getElementById('snackbar');
                  snackbar.innerHTML = "You don't have enough balance to buy this Gig!";
                  snackbar.className = "show";
                  setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
                }
                 // if order is placed successfully...
                else {
                  var url = "{{route('buyer.contactOrder')}}/" + data;
                  console.log(url);
                  var snackbarSuccess = document.getElementById('snackbarSuccess');
                  snackbarSuccess.innerHTML = "Order has been placed successfully!";
                  snackbarSuccess.className = "show";
                  setTimeout(function() {
                    snackbarSuccess.className = snackbarSuccess.className.replace("show", "");
                  }, 3000);
                  window.location.href = url;
                }
              }
            });
          }

        }*/
</script>
@endauth
@guest
<script>
    function placeOrder(serviceID) {
        window.location = '{{route('login')}}';
      }
</script>
@endguest
@endpush