<!--WHY 10NAR-->
<div class="block-intro-why">
    <div class="container">
        <h6>Pourquoi travailler sur 10nar ? </h6>
        <div class="bg-customize line-text"></div>
        <ul class="row">

            <li class="why-item-1 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="/assets/images/graphics/why-icon-1.png" alt="">
                <p class="title">Crédibilité</p>
                <div class="bg-customize line-text"></div>
                <p class="content" style="padding-top: 10px;">
                    10nar est une communauté qui valorise votre confiance et votre sécurité.
                    Grâce à un système d'évaluation fiable.
                </p>
            </li>
            <li class="why-item-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="/assets/images/graphics/why-icon-2.png" alt="">
                <p class="title">Sécurisé</p>
                <div class="bg-customize line-text"></div>
                <p class="content" style=" padding-top: 10px;">
                    Toutes les transactions sont sécurisées et cryptées.
                </p>
            </li>
            <li class="why-item-3 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="/assets/images/graphics/why-icon-3.png" alt="">
                <p class="title">Communauté</p>
                <div class="bg-customize line-text"></div>
                <p class="content" style=" padding-top: 10px;">
                    C'est une relation tripartite entre Vendeur, Acheteur et la plateforme 10nar.
                </p>
            </li>
        </ul>
    </div>
</div>