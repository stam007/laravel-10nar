<div class="block-slider">
    <div class="slideshow">
        <!--CONTENT SLIDER-->
        <div class="slider-wrapper default">
            <div id="slider">
                <img src="/assets/images/slider/slide-1.jpg" />
                <img src="/assets/images/slider/slide-2.jpg" />
                <img src="/assets/images/slider/slide-3.jpg" />
            </div>
        </div>
    </div>
    <div class="search-form">
        <!--CONTENT FORM SEARCH-->
        <h1>Commander vos micro-services, en ligne à partir de 10Dt</h1>
        <h4>Parcourrez un large catalogue de micro-services et Acheter en toute sécurité & simplicité. </h4>
        <form class="form-search">
            <div class="outer-form-search">
                <span class="text">J'ai besoin de</span>
                <input type="text" name="s" class="text-search-home" placeholder="logo graphique">
                <button class="btn-diplomat btn-find btn- waves-effect waves-light">
                    <div class="search-title"><span class="text-search">Rechercher</span></div>
                </button>
            </div>
        </form>
    </div>
    <div class="statistic-job-number">
        <p class="link-last-job">Plus de 3333 micro-services disponibles</p>
        <div class="bounce"><i class="fas fa-angle-down"></i></div>
        <p></p>
    </div>
</div>