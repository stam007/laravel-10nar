<!--SECTION HOW-->
<div class="block-intro-how">
    <div class="container">
        <h6>Comment fonctionne 10Nar ? </h6>
        <div class="bg-customize line-text"></div>
        <div class="contents">
            <ul class="steps">
                <li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn"
                    style="visibility: visible; animation-name: fadeIn;"><span class="number-steps">1</span>
                </li>
                <li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn"
                    style="visibility: visible; animation-name: fadeIn;"><span class="number-steps">2</span>
                </li>
                <li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn"
                    style="visibility: visible; animation-name: fadeIn;"><span class="number-steps">3</span>
                </li>
            </ul>
            <div class="clearfix"></div>
            <ul class="text-information">

                <li class="how-item-1 col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn"
                    style="visibility: visible; animation-name: fadeIn;">
                    <div class="steps-line"></div>
                    <div class="icon">
                        <div class="hex">
                            <img src="/assets/images/graphics/how-icon-1.png" alt="">
                        </div>
                    </div>
                    <p class="title">Indiquez vos besoins</p>
                    <p class="content">La page d'accueil vous fournira des options avancées de recherche et de
                        filtrage.
                        Il vous permet de faire une recherche par mot-clé, de filtrer par catégories et par
                        tags.
                        D'un simple clic, vous pouvez trouver ce que vous voulez.
                    </p>
                </li>
                <li class="how-item-2 col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn"
                    style="visibility: visible; animation-name: fadeIn;">
                    <div class="steps-line"></div>
                    <div class="icon">
                        <div class="hex">
                            <img src="/assets/images/graphics/how-icon-2.png" alt="">
                        </div>
                    </div>
                    <p class="title">Sélectionnez votre vendeur préféré</p>
                    <p class="content">Après avoir trouvé des services connexes, vous pouvez consulter le profil
                        des vendeurs en vous fournissant
                        des détails sur leur biographie et tous leurs micro-services. Ensuite, sélectionnez
                        celui qui correspond à vos besoins.
                    </p>
                </li>
                <li class="how-item-3 col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn"
                    style="visibility: visible; animation-name: fadeIn;">
                    <div class="steps-line"></div>
                    <div class="icon">
                        <div class="hex">
                            <img src="/assets/images/graphics/how-icon-3.png" alt="">
                        </div>
                    </div>
                    <p class="title">Communication</p>
                    <p class="content">Échangez par chat privé sur le site jusqu’à la livraison, Le vendeur
                        n’est payé que lorsque la commande est terminée.
                        Ou
                        communiquez par chat privé sur le site
                        jusqu’à la livraison, Le vendeur n’est payé que lorsque la commande est terminée.
                    </p>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--SECTION BE A SELLER -->
<div class="block-login wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
    <div class="background-opacity"></div>
    <div class="container inner">
        <div class="non-login">
            <p class="main-title">Devenez vendeur</p>
            <p class="sub-title">Proposez gratuitement vos microservices sur 10nar et commencez à gagner de
                l’argent dès maintenant. Une offre simple, sans engagement, et adaptée à tous les vendeurs.</p>
            <a href="https://microjobengine.enginethemes.com/microjobengine-diplomat/sign-in/#register"
                class="btn-diplomat btn-link-site waves-effect waves-light">
                <span class="text">
                    Vendre sur 10nar
                </span>
                <i class="fas fa-long-arrow-right" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>