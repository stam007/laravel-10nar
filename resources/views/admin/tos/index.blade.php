@extends('admin.layout.master')

{{-- All necessary JS for NIC Editor --}}
@push('nic-editor-scripts')
  <script src="{{asset('assets/admin/js/nic-edit/nicEdit.js')}}" type="text/javascript"></script>
  <script type="text/javascript">
    bkLib.onDomLoaded(function() {
          new nicEditor({iconsPath : "{{asset('assets/admin/js/nic-edit/nicEditorIcons.gif')}}"}).panelInstance('area1');
    });
  </script>
@endpush

@section('body')
  <div class="page-content-wrapper">
          <!-- BEGIN CONTENT BODY -->
          <div class="page-content" style="min-height:811px">
              <!-- BEGIN PAGE HEADER-->
              <!-- BEGIN PAGE TITLE-->
              <h3 class="page-title"> <b> Terms & Conditions</b></h3>
              <!-- END PAGE TITLE-->
              <!-- END PAGE HEADER-->
                  <div class="row">
          <div class="col-md-12">
              <div class="portlet light bordered">
                  <div class="portlet-body form">
                      <form class="form-horizontal" method="post" role="form" action="{{route('admin.tos.update')}}">
                          {{csrf_field()}}
                          <div class="form-body">

                              <div class="form-group">
                                  <label class="col-md-12"><strong style="text-transform: uppercase;"> Terms & Conditions</strong></label>
                                  <div class="col-md-12">
                                      <textarea id="area1" class="form-control" rows="15" name="tos">{{$gs->tos}}</textarea>
                                  </div>
                                  @if($errors->has('tos'))
                                      <p class="col-md-12" style="color:red;">{{$errors->first('tos')}}</p>
                                  @else
                                    <br>
                                    <br>
                                  @endif
                              </div>

                              <div class="row">
                                  <div class="col-md-12">
                                      <button type="submit" class="btn blue btn-block btn-lg"><i class="fas fa-send"></i> Update</button>
                                  </div>
                              </div>

                          </div>
                      </form>

                  </div>
              </div>
          </div>
      </div><!---ROW-->


          </div>
          <!-- END CONTENT BODY -->
      </div>
@endsection
