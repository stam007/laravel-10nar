@extends('layouts.landing')

@section('title', 'Mot de passe oublié? | '.$gs->website_title))

@section('content')
<main role="main" class="container mainContent container--extended">
    <div class="login mainBlock mainBlock--pad25">
        <h1 class="mainLogo textAlignCenter marginBottom25px">
            <a href="{{route('users.home')}}">
                <span class=" sr-only">{{$gs->website_title}} / Retour à l'accueil</span>
                <img class='logo-form' src="/assets/images/logo/logo.png" title="{{$gs->website_title}}"
                    alt="{{$gs->website_title}}">
            </a>
        </h1>


        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}" class="fos_user_resetting_request">
            @csrf

            <p class="marginTop0 marginBottom25px">
                Indiquez le nom d’utilisateur ou l’adresse
                <br>e-mail du compte afin de recevoir un e-mail
                <br>et réinitialiser votre mot de passe.
            </p>
            <div class="form-group">
                <label class="sr-only" for="email">E-mail :</label>
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                    value="{{ old('email') }}" placeholder="E-mail" required>
                @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <button type="submit" class="btn-reset btn btn-default btn-medium width100p">
                Réinitialiser
            </button>
        </form>
    </div>
</main>
@endsection