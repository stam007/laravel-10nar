@extends('layouts.landing')

@section('title', 'Connexion | '.$gs->website_title))

@section('content')

<main role="main" class="container mainContent container--extended">
    <div class="login mainBlock">

        <h1 class="mainLogo textAlignCenter marginBottom25px">
            <a href="{{route('users.home')}}">
                <span class=" sr-only">{{$gs->website_title}} / Retour à l'accueil</span>
                <img class='logo-form' src="/assets/images/logo/logo.png" title="{{$gs->website_title}}"
                    alt="{{$gs->website_title}}">
            </a>
        </h1>

        @if (!$errors->isEmpty())
        <div class="alert alert_danger alert_sm" style="animation-delay: .1s">
          <div class="alert--icon">
            <i class="fas fa-times-circle"></i>
          </div>
          <div class="alert--content">
            @foreach ($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
          </div>
          <div class="alert--close">
            <i class="far fa-times-circle"></i>
          </div>
        </div>
        <p class="marginBottom0">         
        </p>
        @endif

        @if(session('success'))
        <div class="alert alert_success alert_sm" style="animation-delay: .1s">
          <div class="alert--content">
            <p>{{session('success')}}</p>
          </div>
        </div>
        <p class="marginBottom0">         
        </p>
        @endif

        <div class="grid-Form buttonsConnects buttonsConnects--border">
            <a role="button" class=" grid-column-fullCell btn btn-connect" href="#">
                <span>
                    <svg fill="#fff" width="10" height="18" viewBox="0 0 10 18" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M9.623.004l-2.399-.004c-2.695 0-4.436 1.739-4.436 4.43v2.042h-2.412c-.208 0-.377.164-.377.367v2.959c0 .203.169.367.377.367h2.412v7.467c0 .203.169.367.377.367h3.146c.208 0 .377-.164.377-.367v-7.467h2.82c.208 0 .377-.164.377-.367l.001-2.959c0-.097-.04-.191-.11-.26-.071-.069-.167-.108-.267-.108h-2.821v-1.731c0-.832.204-1.255 1.318-1.255l1.616-.001c.208 0 .377-.164.377-.367v-2.748c0-.202-.169-.367-.377-.367z"
                            fill="inherit"></path>
                    </svg>

                </span>
                Connexion avec Facebook
            </a>
            <a class=" grid-column-fullCell btn btn-connect btn-connect--google" href="#">
                <span>
                    <svg fill="#fff" width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M7 6v2.4h3.97c-.16 1.03-1.2 3.02-3.97 3.02-2.39 0-4.34-1.98-4.34-4.42s1.95-4.42 4.34-4.42c1.36 0 2.27.58 2.79 1.08l1.9-1.83c-1.22-1.14-2.8-1.83-4.69-1.83-3.87 0-7 3.13-7 7s3.13 7 7 7c4.04 0 6.72-2.84 6.72-6.84 0-.46-.05-.81-.11-1.16h-6.61z"
                            fill="inherit"></path>
                    </svg>

                </span>
                Connexion avec Google
            </a>
        </div>

        <form method="POST" action="{{ route('login') }}" class="formNoLabel">
            @csrf
            <div class="grid-Form">
                <div class="grid-column-fullCell">
                    <label class="sr-only" for="email">E-mail</label>
                    <input placeholder="E-mail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        id="email" type="email" name="email" value="{{ old('email') }}" required="required"
                        autofocus="autofocus">
                </div>

                <div class="grid-column-fullCell positionRelative">
                    <label class="sr-only" for="password">Mot de passe <a tabindex="-1" href="/mot-de-passe/oublie"
                            class="iforgot">(Oublié ?)</a></label>
                    <input placeholder="Mot de passe" id="password" type="password"
                        class="form-control form-control--forgotpassword{{ $errors->has('password') ? ' is-invalid' : '' }}"
                        name="password" required>
                    <a href="{{ route('password.request') }}">Oublié ?</a>
                </div>
                <div class="grid-column-fullCell positionRelative">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Se souvenir
                        de moi
                    </label>
                </div>
                <div class="grid-column-fullCell">
                    <button type="submit" class="btn btn-default btn-medium width100p" name="_submit">
                        Connexion
                    </button>
                </div>
            </div>
        </form>

        <p>
            Pas encore membre ?
            <a href="/inscription">Inscrivez-vous</a>.
        </p>
    </div>
</main>
@endsection