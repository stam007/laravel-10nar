@extends('layouts.landing')

@section('title', 'Inscription | '.$gs->website_title))

@push('after-scripts')
<script>
$(document).ready(function() {
  
  if($("input:radio[id='personal_company']").is(':checked') || $("input:radio[id='company']").is(':checked')) {
      $("#companyDetails").show();
    }
    else if ($("input:radio[id='personal']").is(':checked')) $("#companyDetails").hide();

    $("input[name='account_type']").on("change", function() {
        console.log($(this).val());
        if ($(this).val() === 'Entreprise Individuelle' || $(this).val() === 'Société') {
            $("#companyDetails").show();
        } else $("#companyDetails").hide();
    });
});
</script>
@endpush


@section('content')
<main role="main">
  <div class="container mainContent container--extended">
    <div class="register">
      <div class="mainBlock mainBlock--pad25">
        <h1 class="mainLogo textAlignCenter marginBottom25px">
          <a href="{{route('users.home')}}">
            <span class=" sr-only">{{$gs->website_title}} / Retour à l'accueil</span>
            <img class='logo-form' src="/assets/images/logo/logo.png" title="{{$gs->website_title}}"
              alt="{{$gs->website_title}}">
          </a>
        </h1>


        @if (!$errors->isEmpty())
        <div class="alert alert_danger alert_sm" style="animation-delay: .1s">
          <div class="alert--icon">
            <i class="fas fa-times-circle"></i>
          </div>
          <div class="alert--content">
            @foreach ($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
          </div>
          <div class="alert--close">
            <i class="far fa-times-circle"></i>
          </div>
        </div>
        @endif
        @if (config('app.registration') === false)
        <div class="alert alert_warning alert_sm" style="animation-delay: .1s">
          <div class="alert--icon">
            <i class="fas fa-exclamation-circle"></i>
          </div>
          <div class="alert--content">
            <p>Les inscriptions sont désactivées</p>
          </div>
        </div>
        @else
        <p class="marginBottom0">         
        </p>
        <form action="{{route('register')}}" method="post">
          {{csrf_field()}}
          <p class="marginBottom0">
            Afin de confirmer votre inscription, veuillez indiquer votre adresse e-mail. Choisissez également un
            nom d’utilisateur, qui vous permettra de vous connecter et sera affiché sur vos achats et ventes.
          </p>
          <h2 class="title-L">Type de compte</h2>
          <group class="inline-radio">
            <div><input id='personal' type="radio" name="account_type" value="Particulier" @if(old('account_type') == 'Particulier') checked @endif><label>Particulier</label></div>
            <div><input id='personal_company' type="radio" name="account_type" value="Entreprise Individuelle" @if(old('account_type') == 'Entreprise Individuelle') checked @endif><label>Entreprise Individuelle
                (Patente)</label></div>
            <div><input id='company' type="radio" name="account_type" value="Société" @if(old('account_type') == 'Société') checked @endif><label>Société</label></div>
          </group>
          <h2 class="title-L">Votre compte</h2>
          <div id="companyDetails" class="grid-Form" style="display:none;">
            <div>
              <label class="sr-only control-label required" for="company_name">Nom de l'Entreprise</label>
              <input type="text" id="firstname" placeholder="Nom de l'Entreprise" class="form-control"
                name="company_name" value="{{old('company_name')}}">
              @if ($errors->has('company_name'))
              <span style="color:red;">
                <strong>{{ $errors->first('company_name') }}</strong>
              </span>
              @endif
            </div>
            <div>
              <label class="sr-only control-label required" for="tax_registration">Matricule fiscale</label>
              <input type="text" id="firstname" placeholder="Matricule fiscale" class="form-control"
                name="tax_registration" value="{{old('tax_registration')}}">
              @if ($errors->has('tax_registration'))
              <span style="color:red;">
                <strong>{{ $errors->first('tax_registration') }}</strong>
              </span>
              @endif
            </div>
            <span class="marginBottom20px"></span>
          </div>

          <div class="grid-Form">
            <div>
              <label class="sr-only control-label required" for="firstname">Prénom</label>
              <input type="text" id="firstname" placeholder="Prénom" class="form-control" name="firstname"
                value="{{old('firstname')}}">
              @if ($errors->has('firstname'))
              <span style="color:red;">
                <strong>{{ $errors->first('firstname') }}</strong>
              </span>
              @endif
            </div>
            <div>
              <label class="sr-only control-label required" for="lastname">Nom</label>
              <input type="text" id="lastname" placeholder="Nom" class="form-control" name="lastname"
                value="{{old('lastname')}}">
              @if ($errors->has('lastname'))
              <span style="color:red;">
                <strong>{{ $errors->first('lastname') }}</strong>
              </span>
              @endif
            </div>
            <span class="marginBottom20px"></span>
          </div>

          <div class="grid-Form">
            <div>
              <label class="sr-only control-label required" for="email">E-Mail</label>
              <input type="email" id="email" placeholder="E-Mail" class="form-control" name="email"
                value="{{old('email')}}">
              @if ($errors->has('email'))
              <span style="color:red;">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
            <div>
              <label class="sr-only control-label required" for="name">Nom d'utilisateur</label>
              <input type="text" id="name" placeholder="Nom d'utilisateur" class="form-control" name="name"
                value="{{old('name')}}" maxlength="16" pattern=".{3,}">
              @if ($errors->has('name'))
              <span style="color:red;">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <h3 class="title-M">Mot de passe</h3>
          <div class="grid-Form">
            <div>
              <label class="sr-only control-label required" for="password">Mot de passe</label>
              <input type="password" id="password" placeholder="Mot de passe" class="form-control" name="password"
                value="{{old('password')}}">
              @if ($errors->has('password'))
              <span style="color:red;">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
            <div>
              <label class="sr-only control-label required" for="password_confirmation">Mot de passe</label>
              <input type="password" id="password_confirmation" placeholder="Confirmation du mot de passe"
                class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
              @if ($errors->has('password_confirmation'))
              <span style="color:red;">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <h3 class="title-M hint--right hint--large"
            aria-label="À titre d’information uniquement et afin de vous accueillir au mieux, indiquez-nous la raison principale de votre inscription. Vous n’aurez pas besoin de créer un autre compte si vous souhaitez passer d’une activité à l’autre.">
            Type de profil&nbsp;<i class="fas fa-info-circle"></i></h3>
          <group class="inline-radio">
            <div><input type="radio" name="wish" value="buy" @if(old('wish') == 'buy') checked @endif><label>Je souhaite acheter</label></div>
            <div><input type="radio" name="wish" value="sell" @if(old('wish') == 'sell') checked @endif><label>Je souhaite vendre</label></div>
            <div><input type="radio" name="wish" value="both" @if(old('wish') == 'both') checked @endif><label>Les deux !</label></div>
          </group>
          <div class="block-Grey marginTop25px">
            <div class="form-group form-check paddingBottom0">
              <input type="checkbox" name="tos" class="form-check-input flexShrink0" @if(old('tos')) checked @endif>
              <label class="form-check-label fontWeight400" for="registration_cgu">
                J’ai lu et j’accepte les <a href="/cgu" rel="noopener noreferrer" target="_blank">conditions générales
                  d’utilisation</a>.
              </label>
            </div>
          </div>
          <div class="btn-center">
            <button type="submit" class="btn btn-default btn-medium " name="_submit">
              Valider mon inscription
            </button>
          </div>
        </form>
        @endif
      </div>
    </div>
  </div>
</main>
@endsection