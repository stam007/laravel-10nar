@extends('layouts.app')

@push('after-scripts')

@endpush

@push('styles')
<link rel='stylesheet' href='/assets/css/become_seller.css' type='text/css' media='all' />

<style>



.item-1, 
.item-2{
    position: absolute;
    text-align: center;
  display: block;
top:50%;
transform: translate(-50%, -50%) ; 
  width: 60%;
  
  font-size: 2em;

	animation-duration: 15s;
	animation-timing-function: ease-in-out;
	animation-iteration-count: infinite;
}

.item-1{
	animation-name: anim-1;
}

.item-2{
	animation-name: anim-2;
}


@keyframes anim-1 {
	0%, 10% { left: -100%; opacity: 0; }
  10%,40% { left: 50%; opacity: 1; }
  50%, 100% { left: 110%; opacity: 0; }
}

@keyframes anim-2 {
	0%, 50% { left: -100%; opacity: 0; }
  60%, 90% { left: 50%; opacity: 1; }
  100% { left: 110%; opacity: 0; }
}




.title-logo{
    vertical-align: middle;
    width: 86px;
    padding-bottom: 16px;
    padding-left: 1px;
    }

    .user {
      text-align: right;
      white-space: nowrap;
      display: inline;
      float: right;
      margin-right: 30px;
      font-size: large;
    }


/* Toggle */

.pricing-switcher {
  text-align: center;
}
.pricing-switcher .fieldset {
  display: inline-block;
  position: relative;
  padding: 5px;
  border-radius: 50em;
  background-color: lightgray;
  height: 50px;
}
.pricing-switcher input[type="radio"] {
  position: absolute;
  opacity: 0;
}
.pricing-switcher label {
  position: relative;
  z-index: 1;
  display: inline-block;
  float: left;
  width: 150px;
  height: 40px;
  line-height: 40px;
  cursor: pointer;
  color: #4E9DD5;
  text-transform: uppercase;
  text-decoration: none;
  font-weight: bold;
  transition-delay: .1s;
  transition: color 0.3s ease;
}
.pricing-switcher input[type="radio"]:checked + label {
  color: #FFFFFF;
  text-decoration: none;
  z-index:2;
}
.pricing-switcher .switch {
  /* floating background */
  position: absolute;
  top: 5px;
  left: 5px;
  height: 40px;
  width: 150px;
  background-color: #4E9DD5;
  border-radius: 50em;
  transition: transform 0.3s;
}
.pricing-switcher input[type="radio"]:checked + label + .switch,
.pricing-switcher input[type="radio"]:checked + label:nth-of-type(n) + .switch {
  /* use label:nth-of-type(n) to fix a bug on safari with multiple adjacent-sibling selectors*/
  -webkit-transform: translateX(150px);
  -moz-transform: translateX(150px);
  -ms-transform: translateX(150px);
  -o-transform: translateX(150px);
  transform: translateX(150px);
}


    </style>
@endpush

@section('meta-ajax')
<meta name="_token" content="{{ csrf_token() }}" />
@endsection

@section('title', config('app.name') . ' | ' . 'La première platefome de mico-services en Tunisie.' )

@section('content')

<main role="main" class="container">

        <div class="pricingHeader">
            <span class="item-1">Boostez vos ventes avec nos formules <b>10NAR<sup>+</sup></b> et <b>10NAR<sup>PRO</sup></b></span>
            <span class="item-2">Une offre adaptée aux besoins de chacun et sans engagement.</span>
        </div>

        <div class="pricingAvantages">
            <h2 class="titleHome">
                Les avantages de
                <img class="title-logo" src="/assets/images/logo/logo.png"/>
            </h2>

            <p class="subTitle">
                Pour tous nos vendeurs, nous garantissons…
            </p>
            <ul class="pricingGarantee">
                <li><span>✓</span> Une offre gratuite, toujours disponible, <strong>sans limite de revenus</strong> ;
                </li>
                <li><span>✓</span> Une <strong>commission flexible</strong>, adaptée à vos besoins ;</li>
                <li><span>✓</span> Un retrait des gains possible à partir de 48 dinars ;</li>
                <li><span>✓</span> Une visibilité en quelques clics et des premières ventes rapidement.</li>
            </ul>
        </div>

        <div class="pricingWrapper js-pricingWrapper">
            <h2 class="titleHome">
                Une formule sur-mesure
            </h2>
            <p class="subTitle">
                Des offres simples, sans engagement, et adaptées à tous
            </p>
            <div class="pricing-switcher">
                <div class="fieldset">
                  <input type="radio" name="group-a" value="monthly" id="monthly-1" checked>
                  <label for="monthly-1">Mensuelle</label>
                  <input type="radio" name="group-a" value="annualy" id="annualy-1">
                  <label for="annualy-1">Annuelle</label>
                  <span class="switch"></span>

              </div>
            <header class="pricingSwitch">

                  <p>
                    2 mois offerts avec l’offre annuelle !
                </p>
             </div>

             <!--
                <div class="col col--1">
                    <label for="switchSubscriptionFrequency" class="switchTitle">
                        Mensuelle
                    </label>
                </div>
                <div class="col-switch">
                    <label class="switchWrapper switchWrapper--hasBackground">
                        <input id="switchSubscriptionFrequency" type="checkbox" class="js-switchPrices">
                        <div class="switch"></div>
                    </label>
                </div>
                <div class="col col col--2">
                    <label for="switchSubscriptionFrequency" class="switchTitle">
                        Annuelle
                    </label>

                </div>
            -->
            </header>
        </div>
        <div class="pricingCols">
            <div class="row">
                <div class="col-md-4">
                    <div class="pricingCol">
                        <div class="pricingCol__top">
                            <h2 class="pricingCol-Title">
                                <span>
                                    Pour commencer
                                </span>
                            </h2>

                            <div class="pricingPrice pricingPrice--free">
                                <span class="pricingAmount">Gratuit</span>
                            </div>

                            <div class="pricingPrice__description">
                                Vendez vos premiers microservices.
                            </div>

                            <div>
                                <a href="/inscription" class="btn btn-default">Inscription</a>

                            </div>

                            <div class="pricingPrice__help">
                                <p>Sans engagement, gratuit à vie.</p>

                            </div>
                        </div>
                        <div class="pricingCol__bottom">
                            <ul class="pricingDetails">
                                <li>
                                    <span>
                                        Commission de <strong>15 %</strong> par vente
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <strong>5</strong> microservices maximum
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <strong>5</strong> options max. par microservice
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Jusqu’à <strong>100 DT</strong> par option
                                    </span>
                                </li>
                                <li>
                                    <del>
                                        Statistiques détaillées
                                    </del>
                                </li>
                                <li>
                                    <del>
                                        Badge vendeur vérifié 
                                    </del>
                                </li>
                                <li>
                                    <del>
                                        Visibilité accrue sur 10nar 
                                    </del>
                                </li>
                                <li>
                                    <del>
                                        Mise en forme des microservices
                                    </del>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pricingCol">
                        <div class="pricingCol__top">
                            <h2 class="pricingCol-Title">
                                10nar <sup>+</sup>
                            </h2>

                            <div class="pricingPrice">
                                <span class="positionRelative" style="font-size: 0;">
                                    <span class="js-amount pricingAmount" data-monthly-amount="5"
                                        data-yearly-amount="100">10</span><sup>TND <span>TTC</span></sup>
                                </span><span class="pricingPrice__frequency"> / <span data-month="Mois" data-year="An"
                                        class="js-duration">Mois</span>
                                </span>
                            </div>

                            <div class="pricingPrice__description">
                                Boostez vos ventes et générez plus de revenus.
                            </div>

                            <div>
                                <form action="/inscription" method="post">
                                    <div style="display: none;">
                                        <input type="radio" name="subscription[frequency]" value="month"
                                            checked="checked">
                                        <input type="radio" name="subscription[frequency]" value="year">
                                    </div>

                                    <input type="hidden" name="subscription[_token]"
                                        value="m1tf4QoL0BJi8OJMDwKTzo1TbunIU3o6iDkpMei0yrc">
                                    <button id="subscriptionSubmit" class="btn btn-default">Essayer
                                        gratuitement</button>
                                </form>

                            </div>

                            <div class="pricingPrice__help">
                                <p>Sans engagement, 30 jours d’essai gratuit.</p>
                            </div>
                        </div>
                        <div class="pricingCol__bottom">
                            <ul class="pricingDetails">
                                <li>
                                    <span>
                                        Commission <strong>7%</strong> par vente
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <strong>20</strong> microservices maximum                                   
                                     </span>
                                </li>
                                <li>
                                    <span>
                                        <strong>10</strong> options max. par microservice
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Jusqu’à <strong>200 DT</strong> par option
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Modification du pseudonyme
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Statistiques détaillées
                                    </span>
                                </li>
                                <li>
                                    <del>
                                        Badge vendeur vérifié 
                                    </del>
                                </li>
                                <li>
                                    <del>
                                        Visibilité accrue sur 10nar 
                                    </del>
                                </li>
                                <li>
                                    <del>
                                        Mise en forme des microservices
                                    </del>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pricingCol">
                        <div class="pricingCol__top">
                            <h2 class="pricingCol-Title">
                                <span>
                                    10nar <sup>Pro</sup>
                                </span>
                            </h2>

                            <div class="pricingPrice">
                                <span class="positionRelative" style="font-size: 0;">
                                    <span class="js-amount pricingAmount" data-monthly-amount="50"
                                        data-yearly-amount="500">50</span><sup>DT <span>HT</span></sup>
                                </span><span class="pricingPrice__frequency"> / <span data-month="Mois" data-year="An"
                                        class="js-duration">Mois</span>
                                </span>
                            </div>

                            <div class="pricingPrice__description">
                                Démarquez-vous et professionalisez votre activité.
                            </div>

                            <div>
                                <form action="/inscription" method="post">
                                    <div style="display: none;">
                                        <input type="radio" name="subscription[frequency]" value="month"
                                            checked="checked">
                                        <input type="radio" name="subscription[frequency]" value="year">
                                    </div>

                                    <input type="hidden" name="subscription[_token]"
                                        value="m1tf4QoL0BJi8OJMDwKTzo1TbunIU3o6iDkpMei0yrc">
                                    <button id="subscriptionSubmit" class="btn btn-default">Essayer
                                        gratuitement</button>
                                </form>

                            </div>

                            <div class="pricingPrice__help">
                                <p>Sans engagement, 30 jours d’essai gratuit.</p>
                            </div>
                        </div>
                        <div class="pricingCol__bottom">
                            <ul class="pricingDetails">
                                <li>
                                    <span>
                                        Commission de <strong>5%</strong> par vente
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Microservices <strong>illimités</strong>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <strong>20</strong> options max. par microservice
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Jusqu’à <strong>400 DT</strong> par option
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Modification du pseudonyme
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Statistiques détaillées
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Badge vendeur vérifié 
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Visibilité accrue sur 10nar 
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Mise en forme des microservices
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
@endsection

{{-- 
@section('content')
  <div class="widget__title card__header card__header--has-btn">
     <div class="widget_title1">
        <h4>All Services</h4>
     </div>
  </div>
  <br><br>

  <div class="list-group">
    @foreach ($services as $service)
      <div class="media single-service" style="padding:0px 40px;">
        <div class="service-image-container">
          <a href="#">
            <img class="media-object" src="{{asset('_assets/users/service_images/'.$service->serviceImages()->first()->image_name)}}"
alt="...">
@if ($service->feature == 1)
<h5><span style="display:block;" class="label label-primary">Featured</span></h5>
@endif
</a>
</div>
<div class="media-body">
    <div class="media-heading-price-container">
        <h2 class="media-heading"><a href="{{route('services.show', [$service->id, $service->user->id])}}"
                class="text-primary">{!!(strlen($service->service_title)>40) ? substr($service->service_title, 0, 40) .
                '...' : $service->service_title!!}</a></h2>
        <small class="pull-right text-danger"><strong>{{$service->price}} {{$gs->base_curr_symbol}}</strong></small>
    </div>
    <p style="clear:both;"></p>
    <div class="col-md-12 description-username-container">
        <p class="service-description col-md-10">
            {!!(strlen(strip_tags($service->description))>120) ? substr(strip_tags($service->description), 0, 120) .
            '...' : strip_tags($service->description)!!}
        </p>
        <small class="col-md-2 username"><strong><a class="text-danger"
                    href="{{route('users.profile', $service->user->id)}}">{{$service->user->username}}</a></strong></small>
    </div>
    <div class="row">
        <div class="col-md-6">
            <button style="margin-right:10px;" class="btn btn-sm btn-primary pull-left" type="button" name="button"><i
                    class="fas fa-thumbs-o-up" aria-hidden="true"></i>
                {{count($service->orders()->where('like', 1)->get())}}</button>
            <button class="btn btn-sm btn-warning pull-left" type="button" name="button"><i class="fas fa-thumbs-o-down"
                    aria-hidden="true"></i> {{count($service->orders()->where('like', 0)->get())}}</button>
        </div>
        <div class="col-md-6">
            <button class="btn btn-danger btn-sm pull-right" class="pull-right" onclick="placeOrder({{$service->id}})">
                <i class="fas fa-shopping-cart" aria-hidden="true"></i>
                Order Now
            </button>
        </div>
    </div>
</div>
@if (!$loop->last)
<hr>
@endif
</div>
@endforeach
<div class="row">
    <div class="text-center">
        {{$services->links()}}
    </div>
</div>
</div>

@component('users.components.balanceShortage')
@endcomponent
@component('users.components.success')
@endcomponent
@endsection
--}}



@push('scripts')
@auth
<script>
    function placeOrder(serviceID) {

          var fd = new FormData();
          fd.append('serviceID', serviceID);
          $.ajaxSetup({
              headers: {
                  'X-CSRF-Token': $('meta[name=_token]').attr('content')
              }
          });
          // console.log(token + ' ' + serviceID);
          var c = confirm('Are you sure you want to place this order?');

          if(c == true) {
            $.ajax({
              url: '{{route('buyer.placeOrder')}}',
              type: 'POST',
              data: fd,
              contentType: false,
              processData: false,
              success: function(data) {
                // if user balance runs into shortage...
                console.log(data);
                if(typeof data.balance != 'undefined') {
                  var snackbar = document.getElementById('snackbar');
                  snackbar.innerHTML = "You don't have enough balance to buy this Gig!";
                  snackbar.className = "show";
                  setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
                }
                 // if order is placed successfully...
                else {
                  var url = "{{route('buyer.contactOrder')}}/" + data;
                  console.log(url);
                  var snackbarSuccess = document.getElementById('snackbarSuccess');
                  snackbarSuccess.innerHTML = "Order has been placed successfully!";
                  snackbarSuccess.className = "show";
                  setTimeout(function() {
                    snackbarSuccess.className = snackbarSuccess.className.replace("show", "");
                  }, 3000);
                  window.location.href = url;
                }
              }
            });
          }

        }
</script>
@endauth
@guest
<script>
    function placeOrder(serviceID) {
        window.location = '{{route('login')}}';
      }
</script>
@endguest
@endpush