<!DOCTYPE html>
<html lang="fr-FR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1 ,user-scalable=no">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title',$gs->website_title)</title>
    <meta name="description" content="@yield('meta_description', '')">
    <meta name="author" content="@yield('meta_author', '10NAR')">
    @yield('meta')
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <link rel='stylesheet' href='/assets/css/vendor.css?ver=1.2' type='text/css' media='all' />
    <link rel='stylesheet' href='/assets/css/main.css?ver=4.6.3' type='text/css' media='all' />
    <link rel='stylesheet' href='/assets/css/custom.css?ver=4.6.3' type='text/css' media='all' />
    <link rel='stylesheet' href='/assets/fontawesome-free/css/all.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='/assets/css/hint.min.css' type='text/css' media='all' />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155156325-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-155156325-1');
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type='text/javascript' src='/assets/js/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript' src='/assets/js/slider.js'></script>
    <script type='text/javascript' src='/assets/js/slider-bt.js'></script>
    @stack('styles')
    @stack('scripts')
    @stack('nic-editor-scripts')
</head>

<body class="_10nar home">
    <header id="_10nar-header">
        @include('includes.header')
        @include('includes.categories')
    </header>
    @yield('slider')
    @yield('top-static-content')
    @yield('content')

    <!--SECTION FOOTER -->
    @include('includes.footer')
    <script type='text/javascript' src='/assets/js/bootstrap.min.js?ver=1.2'></script>
    <script type='text/javascript' src='/assets/js/chosen.jquery.min.js?ver=1.2'></script>
    <script type='text/javascript' src='/assets/js/front.js?ver=1.2'></script>
    <script type='text/javascript' src='/assets/js/waves.js?ver=1.2'></script>
    <script type='text/javascript' src='/assets/js/jquery.dot.js?ver=1.2'></script>
    <script type='text/javascript' src='/assets/js/wow.min.js?ver=1.2'></script>
    <script type='text/javascript' src='/assets/js/customscrollbar.min.js?ver=1.2'></script>
    <script type='text/javascript' src='/assets/js/autosize.min.js?ver=1.2'></script>
    <script type='text/javascript' href='/assets/fontawesome-free/js/all.min.js'></script>
</body>

</html>