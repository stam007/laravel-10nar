<!DOCTYPE html>
<html lang="fr-FR">

<head>
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
    <link rel="manifest" href="/icons/site.webmanifest">
    <link rel="mask-icon" href="/icons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', config('app.name'))</title>
    @yield('meta')
    @stack('before-styles')
    <link rel='stylesheet' href='/assets/css/landing.css' type='text/css' media='all' />
    <link rel='stylesheet' href='/assets/fontawesome-free/css/all.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='/assets/css/hint.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='/assets/css/alerts-css.min.css' type='text/css' media='all' />
    @stack('after-styles')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155156325-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-155156325-1');
    </script>
</head>

<body class="bodyLogin">
    <div id="particles-js"></div>
    @yield('content')
    <!-- Scripts -->
    @stack('before-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type='text/javascript' href='/assets/fontawesome-free/js/all.min.js'></script>
    <script type='text/javascript' href='/assets/js/alerts.min.js'></script>
    <script src='https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js'></script>
    <script>
        $(document).ready((function(){const e=document.querySelectorAll(".fa-times-circle"),t=document.querySelectorAll(".alert-fade"),n={attributes:!0};e.forEach(e=>e.addEventListener("click",()=>{(function e(t){return t.classList.contains("alert")?t:e(t.parentNode)})(e).classList.toggle("alert_none")}));const a=new Event("classChange");function s(){const e=new MutationObserver(t=>{const n=t[0].target;e.disconnect(),n.classList.contains("alert_none")||n.dispatchEvent(a)});return e}t.forEach(e=>{s().observe(e,n)}),t.forEach(e=>e.addEventListener("classChange",e=>{const t=e.target;if(!t.classList.contains("alert_none")){const e=1e3*parseInt(t.getAttribute("data-fade-time"));setTimeout(()=>{t.classList.toggle("alert_none");s().observe(t,n)},e)}})),particlesJS("particles-js",{particles:{number:{value:80,density:{enable:!0,value_area:800}},color:{value:"#62B7D7"},shape:{type:"circle",stroke:{width:0,color:"#62B7D7"},polygon:{nb_sides:5},image:{src:"img/github.svg",width:100,height:100}},opacity:{value:.5,random:!1,anim:{enable:!1,speed:1,opacity_min:.1,sync:!1}},size:{value:3,random:!0,anim:{enable:!1,speed:40,size_min:.1,sync:!1}},line_linked:{enable:!0,distance:150,color:"#62B7D7",opacity:.4,width:1},move:{enable:!0,speed:6,direction:"none",random:!1,straight:!1,out_mode:"out",bounce:!1,attract:{enable:!0,rotateX:600,rotateY:1200}}},interactivity:{detect_on:"canvas",events:{onhover:{enable:!0,mode:"repulse"},onclick:{enable:!0,mode:"push"},resize:!0},modes:{grab:{distance:400,line_linked:{opacity:1}},bubble:{distance:400,size:40,duration:2,opacity:8,speed:3},repulse:{distance:200,duration:.4},push:{particles_nb:4},remove:{particles_nb:2}}},retina_detect:!0})}));    
        (function ($) {
    var initialContainer = $('.customcolumns'),
        columnItems = $('.customcolumns li'),
        columns = null,
        column = 0;
    function updateColumns() {
        column = 0;
        columnItems.each(function (idx, el) {
            if ($(columns.get(column)).find('li').length >= (columnItems.length / initialContainer.data('columns'))) {
                column += 1;
            }
            $(columns.get(column)).append(el);
        });
    }
    function setupColumns() {
        columnItems.detach();
        while (column++ < initialContainer.data('columns')) {
            initialContainer.clone().insertBefore(initialContainer);
            column++;
        }
        columns = $('.customcolumns');
        updateColumns();
    }

    $(setupColumns);
})(jQuery);
    </script>
    @stack('after-scripts')
</body>
</html>