<!DOCTYPE html>
<html lang="fr-FR">

<head>
  <meta charset="utf-8">
  <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
  <link rel="manifest" href="/icons/site.webmanifest">
  <link rel="mask-icon" href="/icons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-config" content="/icons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{'Conditions générales | '.$gs->website_title}}</title>
  @yield('meta')
  @stack('before-styles')
  <link rel='stylesheet' href='/assets/css/landing.css' type='text/css' media='all' />
  <link rel='stylesheet' href='/assets/fontawesome-free/css/all.min.css' type='text/css' media='all' />
</head>

<body class="bodyTos">
  <main role="main">
    <div class="container mainContent">
      <div class="register">
        <div class="mainBlock">
          <h1 class="mainLogo textAlignCenter marginBottom25px">
            Conditions générales
          </h1>

          <div>
            {!! $gs['tos'] !!}
          </div>
        </div>
      </div>
    </div>
  </main>
</body>

</html>