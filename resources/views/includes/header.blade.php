<div class="_10nar-pull-top">
    <div class="header_container">
        <!--Logo-->
        <div class="header-left">
            <div id="logo-site">
                <a href="{{route('users.home')}}">
                    <img class="visible-xs" src="/assets/images/logo/logo-app.png" title="{{$gs->website_title}}"
                        alt="{{$gs->website_title}}">
                    <!-- this one will be visible on mobile -->
                    <img class="hidden-xs" src="/assets/images/logo/logo.png" title="{{$gs->website_title}}"
                        alt="{{$gs->website_title}}">
                    <!-- this one will be visible on everything else -->
                </a>
            </div>

            <div class="search-bar">
                <form action="http://microjobengine.enginethemes.com/microjobengine-diplomat" class="et-form">
                    <span class="icon-search"><i class="fas fa-search"></i></span>
                    <input type="text" name="s" id="input-search">
                </form>
            </div>
        </div>
        <!--Function right-->
        <div class="header-right">
            @guest
            <div class="mainMenu float-right header-right">
                <a class="mainMenu__item" href="{{ route('register_method') }}">Inscription</a>
                <a class="mainMenu__item" href="{{ route('login') }}">Connexion</a>
                <a class="mainMenu__item btn-seller d-none d-sm-block" href="{{ route('become_seller') }}">Devenez vendeur</a>
            </div>
            @endguest
            @auth
            <!--
            <div class="notification-icon list-message _10nar-dropdown">
                <span id="show-notifications" class="link-message">
                    <i class="fas fa-bell"></i>
                </span>
            </div>
            -->
            <div class="message-icon list-message dropdown _10nar-dropdown">
                <div class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                    <span class="link-message">
                        <i class="fas fa-comment"></i>
                    </span>
                </div>
                <div class="list-message-box dropdown-menu" aria-labelledby="dLabel">
                    <div class="list-message-box-header">
                        <span>
                            <span class="unread-message-count">0</span> Nouveau </span>
                        <a href="#" class="mark-as-read">Marquer comme lu</a>
                    </div>

                    <ul class="list-message-box-body">

                        <!--
                        <li class="clearfix conversation-item">
                            <div class="inner ">
                                <a href="http://microjobengine.enginethemes.com/microjobengine-diplomat/blog/ae_message/conversation-by-brandon-smith/"
                                    class="link"></a>
                                <div class="img-avatar">
                                    <a href="http://microjobengine.enginethemes.com/microjobengine-diplomat/author/percethorne/"
                                        target="_blank" title="Perce Thorne"><img
                                            src="https://cdn.enginethemes.com/microjobengine/2016/03/cropped-2.jpg"
                                            class="avatar" alt="" /></a> </div>
                                <div class="conversation-text">
                                    <span class="latest-reply"> You: ihnknmk</span>
                                    <span class="latest-reply-time">12 hours ago</span>
                                </div>
                            </div>
                        </li>

                        <li class="clearfix conversation-item">
                            <div class="inner ">
                                <a href="http://microjobengine.enginethemes.com/microjobengine-diplomat/blog/ae_message/conversation-by-brandon-smith-2/"
                                    class="link"></a>
                                <div class="img-avatar">
                                    <a href="http://microjobengine.enginethemes.com/microjobengine-diplomat/author/gabbywinship/"
                                        target="_blank" title="Gabby Winship"><img
                                            src="https://cdn.enginethemes.com/microjobengine/2016/03/cropped-9.jpg"
                                            class="avatar" alt="" /></a> </div>
                                <div class="conversation-text">
                                    <span class="latest-reply"> You: lo</span>
                                    <span class="latest-reply-time">4 days ago</span>
                                </div>
                            </div>
                        </li>
                    -->
                    </ul>

                    <div class="list-message-box-footer">
                        <a href="#">Voir tout</a>
                    </div>
                </div>
            </div>
            <div class="link-post-services">
            <a href="{{route('services.create')}}">Ajouter un micro-service <div class="plus-circle"><i class="fas fa-plus"></i></div> </a>
            </div>
            <div class="user-account">
                <div class="dropdown _10nar-dropdown">
                    <div class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                        <span class="avatar">
                            <span class="display-avatar"><img src="{{empty(Auth::user()->avatar) ? '/assets/images/default/avatar.png' : Auth::user()->avatar}}" class="avatar" alt="{{Auth::user()->name}}" /></span>
                            <span class="display-name">{{Auth::user()->name}}</span>
                        </span>
                        <span><i class="fas fa-angle-right"></i></span>
                    </div>
                    <ul class="dropdown-menu _10nar-dropdown-login" aria-labelledby="dLabel">
                        <li><a href="#">Tableau de board</a></li>
                        <li><a href="#">Mon Profil</a></li>
                        <li><a href="#">Mes Commandes</a></li>
                        <li><a href="#">Mes Factures</a></li>
                        <li class="post-service-link"><a href="{{route('services.create')}}">Ajout un micro-service <div class="plus-circle"><i class="fas fa-plus"></i></div> </a></li>
                        <li class="get-message-link"><a href="#">Mes Messages</a></li>
                        <li>
                            <a href="#" onclick="document.getElementById('logout-form').submit();"> Déconnexion</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                    <div class="overlay-user"></div>
                </div>
            </div>
            @endauth
        </div>
    </div>
</div>