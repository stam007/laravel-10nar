<div class="_10nar-pull-bottom" id="_10nar-nav">
    <nav>
        <div class="navbar navbar-default megamenu">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse">
                    <span class="sr-only">Changer la navigation</span>
                    <i class="fas fa-bars"></i>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul id="nav" class="nav navbar-nav">
                    @foreach ($categories as $categorie)
                    <li
                        class="menu-item menu-item-type-taxonomy menu-item-object-mjob_category menu-item-has-children dropdown {{ (request()->is("categorie/{$categorie->slug}*")) ? 'active' : '' }}">
                        <a title="{{$categorie->name}}" href="{{"/categorie/{$categorie->slug}"}}"
                            class="dropdown-toggle waves-effect waves-light hvr-shutter-in-vertical"
                            aria-haspopup="true">
                            {{$categorie->name}} <i class="fas fa-caret-down" aria-hidden="true"></i>
                        </a>
                        <ul role="menu" class=" dropdown-menu">
                            <div class="div-main-sub">

                                @foreach ($subcategories as $subcategorie)
                                @if ($subcategorie->category_id === $categorie->id)
                                <li
                                    class="menu-item menu-item-type-taxonomy menu-item-object-mjob_category {{ (request()->is("categorie/{$categorie->slug}/{$subcategorie->slug}")) ? 'active' : '' }}">
                                    <a title="{{$subcategorie->name}}"
                                        href="{{"/categorie/{$categorie->slug}/{$subcategorie->slug}"}}"
                                        class="waves-effect waves-light hvr-shutter-in-vertical">{{$subcategorie->name}}</a>
                                </li>
                                @endif
                                @endforeach
                            </div>
                        </ul>
                    </li>
                    @endforeach
                </ul>
                <div class="overlay-nav"></div>
            </div>
        </div>
    </nav>
</div>