<footer id="footer">
    <div class="_10nar-pull-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div>
                        <div class="mainFooter-title">Contact</div>
                        <p>
                            <i class="fas fa-map-marker" aria-hidden="true"></i>
                            <span class="organization-name">10NAR</span><br>
                            <span class="street-address">Rue 206, N°8 Avenue Khezama Est.</span><br>
                            <span class="postal-code">Sousse 4000</span>
                            <span class="locality">Tunisie</span>
                        </p>
                        <i class="fas fa-phone" aria-hidden="true"></i>
                        <span><a href='tel://+21655005666'>(+216) 55 005 666</a></span>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div>
                        <div class="mainFooter-title">Statistiques</div>
                        <div class="stats">
                            <div class="stat">
                                <span class="statItem">21212</span>
                                commandes <span class="d-sm-none d-md-inline">en cours</span>
                            </div>
                            <div class="stat">
                                <span class="statItem">361796</span>
                                microservices <span class="d-sm-none d-md-inline">livrés</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div>
                        <h3 class="mainFooter-title">Assistance 7j/7</h3>
                        <p class="mainFooter-assistance">
                            Consultez <a class="mainFooter-link" href="#">la FAQ</a>.
                            <br>Contactez-nous sur <i class="fas fa-envelope-o" aria-hidden="true"></i>
                            <a href='mailto:info@10nar.net'>info@10nar.net</a>.
                            Nous vous répondrons en moins de 12&nbsp;heures.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">

                    <div>
                        <h3 class="mainFooter-title">Les avis des clients</h3>
                        <div class="siteAvis">
                            <div class="displayFlex alignItemsCenter">
                                <div class="stars">
                                    <span class="star">
                                        <svg width="15" height="14" aria-hidden="true">
                                            <use class="star__full" xlink:href="#svg-star-full"></use>
                                        </svg>
                                    </span>
                                    <span class="star">
                                        <svg width="15" height="14" aria-hidden="true">
                                            <use class="star__full" xlink:href="#svg-star-full"></use>
                                        </svg>
                                    </span>
                                    <span class="star">
                                        <svg width="15" height="14" aria-hidden="true">
                                            <use class="star__full" xlink:href="#svg-star-full"></use>
                                        </svg>
                                    </span>
                                    <span class="star">
                                        <svg width="15" height="14" aria-hidden="true">
                                            <use class="star__full" xlink:href="#svg-star-full"></use>
                                        </svg>
                                    </span>
                                    <span class="star">
                                        <svg width="15" height="14" aria-hidden="true">
                                            <use class="star__half" xlink:href="#svg-star-half"></use>
                                            <symbol viewBox="0 0 15 14" xmlns="http://www.w3.org/2000/svg"
                                                id="svg-star-half">
                                                <path fill="var(--bright_grey)"
                                                    d="M14.757 6.095l-3.044 2.896.718 4.09a.779.779 0 0 1-.321.768.822.822 0 0 1-.848.06l-3.763-1.931-3.763 1.931a.824.824 0 0 1-.848-.059.777.777 0 0 1-.32-.768l.719-4.09L.242 6.095a.772.772 0 0 1-.204-.805.8.8 0 0 1 .65-.534l4.207-.597L6.778.438A.804.804 0 0 1 7.499 0c.307 0 .586.17.721.438l1.883 3.721 4.208.597c.303.043.555.25.648.534a.77.77 0 0 1-.203.805z">
                                                </path>
                                                <path fill="var(--main_color_link)"
                                                    d="M7.5 11.979L3.737 13.91a.824.824 0 0 1-.848-.059.777.777 0 0 1-.32-.768l.719-4.09L.243 6.096a.772.772 0 0 1-.204-.805.8.8 0 0 1 .65-.534l4.207-.597L6.779.439A.804.804 0 0 1 7.5.001c.307 0 0 11.979 0 11.979z">
                                                </path>
                                            </symbol>
                                        </svg>
                                    </span>

                                </div>
                                <div class="siteAvis__number">
                                    98,8&nbsp;%
                                </div>
                            </div>
                            <div class="siteAvis__wording">
                                d’avis positifs sur 12122 commandes notées par les clients de 10nar.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="_10nar-pull-bottom" style="display: block;">
        <div class="container">
            <div class="mainFooter__bottom">
                <div class="footer__row">
                    <div class="col-md-5 order-md-8">
                        <div class="socials">
                            <span class="social">
                                <a rel="noopener noreferrer" href="https://www.facebook.com/10narnet" target="_blank">
                                    <span class="sr-only">Facebook</span>
                                    <img src="/assets/images/social/facebook.svg" alt="Facebook" height="32px"
                                        width="32px" />
                                </a>
                            </span> <span class="social">
                                <a rel="noopener noreferrer" href="https://www.linkedin.com" target="_blank">
                                    <span class="sr-only">LinkedIn</span>
                                    <img src="/assets/images/social/linkedin.svg" alt="LinkedIn" height="32px"
                                        width="32px" />
                                </a>
                            </span>
                            <span class="social">
                                <a rel="noopener noreferrer" href="https://www.instagram.com/10narnet/" target="_blank">
                                    <span class="sr-only">Instagram</span>
                                    <img src="/assets/images/social/instagram.svg" alt="Instagram" height="32px"
                                        width="32px" />
                                </a>
                            </span>
                            <div class="copyright">
                                Copyright &copy;2020 {{config('app.name')}}.
                                <a class="mainFooter-link" href="/cgu">
                                    Conditions générales
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 order-md-4">
                        <div class="paymentsWrapper">
                            <div class="payments">
                                <img class="js-lazy-image js-lazy-image--handled" alt="Visa"
                                    src="/assets/images/payment/visa.png" width="48" height="30">
                                <img class="js-lazy-image js-lazy-image--handled" alt="Mastercard"
                                    src="/assets/images/payment/master-card.png" width="48" height="30">
                                <img class="js-lazy-image js-lazy-image--handled" alt="Poste Tunisienne"
                                    src="/assets/images/payment/poste.png" width="48" height="30">

                            </div>
                            <p>Nous acceptons le paiement par cartes bancaires ou mandats postaux.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2 order-md-4">
                        <div class="miniLogo">
                            <img class="js-lazy-image js-lazy-image--handled" src="/assets/images/logo/logo-light.png"
                                alt="10nar.net" width="90px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</footer>