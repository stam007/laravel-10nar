<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {
        Admin::create([
            'name' => 'Super Admin',
            'username' => 'admin',
            'email' => 'admin@10nar.net',
            'phone'=> '+21655005666',
            'password' =>  bcrypt('admin'),
        ]);

    }
}
