<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Subcategory;
use Cviebrock\EloquentSluggable\Services\SlugService;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories=[
            'Tech & Développement '=>['Développement Web','Application Web & Mobile','Support & IT','Conversion fichier','Autre'],
            'Rédaction & Traduction '=>['Rédaction web','Articles & Blogs','Correction de textes ','Lettre de motivation & CV ','Rédaction de documents officiels','Amélioration de textes','Scripts','Poésie','Traduction','Autre'],
            'Design & Graphique'=>[ 'Plans 2D & 3D','Logo ',' Flyers & Affiches ','Dessin','Charte graphique ','Carte de Visite','Autre'],
            'Musique & Audio '=>['Compositions ','Mixage & Mastering ','Cours','Voix off','Effets sonores','Paroles','Autre'],
            'Marketing Digital'=>['Réseaux sociaux' ,'SEO','SEM',' Sponsorisation' ,'Campagnes publicitaires','Stratégie marketing',' Email marketing','Autre'],
            'Business'=>['Business Plan ','Excel','PowerPoint ','Analyse de projet ','Slogans et nom de marque','Consulting','Autre'],
            'Gaming'=>['Coaching','Boost','Autre'],
            'Vidéos & animations'=> ['Montage vidéo','Motion design','Vidéo publicitaire','Animation & GIF','Autre',] ,  
            'WordPress'=> ['Installation WP','Plug-ins WP','Personnalisation WP ',' Sécurité WP ','Aide & Consultation','Autre']
        ];
        foreach ($categories as $category => $subCategories) {
            $cate = new Category();
            $cate->name = $category;
            $cate->slug = SlugService::createSlug(Category::class, 'slug', $category);
            $cate->save();
            if ($subCategories != null) {
                foreach ($subCategories as $subCategory) {
                    $subCate = new Subcategory();
                    $subCate->name = $subCategory;
                    $subCate->slug = SlugService::createSlug(Subcategory::class, 'slug', $subCategory);
                    $subCate->category_id = $cate->id;
                    $subCate->save();
                }
            }
        }
    }
}
