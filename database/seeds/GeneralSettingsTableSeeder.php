<?php

use App\GeneralSettings;
use Illuminate\Database\Seeder;

class GeneralSettingsTableSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {


        GeneralSettings::create([
            'website_title' => '10NAR',
            'rate' => '0.0002',
            'base_curr_text' => 'TND',
            'base_curr_symbol' => 'DT', 
            'registration' => '1', 
            'email_verification' => '1', 
            'sms_verification' => '1',
            'decimal_after_pt' => '3', 
            'email_notification' => '1', 
            'sms_notification' => '0', 
            'email_sent_from'=> 'info@10nar.net', 
            'email_template' => '<div class="wrapper" style="background-color: rgb(242, 242, 242);"><table align="center" style="table-layout: fixed; color: rgb(184, 184, 184); font-family: Ubuntu, sans-serif;"><tbody><tr><td class="preheader__snippet" style="padding-top: 10px; padding-bottom: 5px; vertical-align: top; width: 280px;">&nbsp;</td><td class="preheader__webversion" style="padding-top: 10px; padding-bottom: 5px; text-align: right; vertical-align: top; width: 280px;">&nbsp;</td></tr></tbody></table><table id="emb-email-header-container" class="header" align="center" style="table-layout: fixed; margin-left: auto; margin-right: auto;"><tbody><tr><td style="width: 600px;"><div class="header__logo emb-logo-margin-box" style="font-size: 26px; line-height: 32px; color: rgb(195, 206, 217); font-family: Roboto, Tahoma, sans-serif; margin: 6px 20px 20px;"><div id="emb-email-header" class="logo-left" align="left" style="font-size: 0px !important; line-height: 0 !important;"><img src="https://i.ibb.co/ZYQx34B/logo.png" alt="" width="200" height="80" style="height: auto; width: 200px; max-width: 200px;"></div></div></td></tr></tbody></table><table class="layout layout--no-gutter" align="center" style="background-color: rgb(255, 255, 255); table-layout: fixed; margin-left: auto; margin-right: auto; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word;"><tbody><tr><td class="column" style="vertical-align: top; color: rgb(96, 102, 109); line-height: 21px; font-family: sans-serif; width: 600px;"><div style="margin-left: 20px; margin-right: 20px; margin-top: 24px;"><div style="line-height: 10px; font-size: 1px;">&nbsp;</div></div><div style="margin-left: 20px; margin-right: 20px;"><p>Bonjour {{name}}, <br/><br/>{{message}}</p></div><div style="margin-left: 20px; margin-right: 20px;"><br></div><div style="margin-left: 20px; margin-right: 20px; margin-bottom: 24px;"><p class="size-14" style="margin-bottom: 0px; line-height: 21px;"><span style="font-weight: 700;">A très bientôt,<br/>L\'équipe <span style="color:#4E9DD5;">10</span><span style="color:#000000;">NAR</span>.</span></p></div></td></tr></tbody></table></div>', 
            'sms_api' => 'https://api.infobip.com/api/v3/sendsms/plain?user=****&password=*****&sender=MicroLancer&SMSText={{message}}&GSM={{number}}&type=longSMS', 
            'ref_com' => 3.00, 
            'phone' => '+21655005666', 
            'email'=> 'info@10nar.net', 
            'comment_script'=> '<div id=\"fb-root\"></div>\r\n<script>/*(function(d, s, id) {\r\n  var js, fjs = d.getElementsByTagName(s)[0];\r\n  if (d.getElementById(id)) return;\r\n  js = d.createElement(s); js.id = id;\r\n  js.src = \'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=446348272471579&autoLogAppEvents=1\';\r\n  fjs.parentNode.insertBefore(js, fjs);\r\n}(document, \'script\', \'facebook-jssdk\'));*/</script>', 
            'tos' => '' 
        ]);

    }
}
