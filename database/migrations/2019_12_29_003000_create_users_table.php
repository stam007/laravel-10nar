<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('password');
            $table->string('company_name')->nullable();
            $table->string('tax_registration')->nullable();
            $table->string('account_type');
            $table->string('wish');
            $table->string('avatar')->nullable();
            $table->double('balance', 8, 3)->default('0.000');
            $table->integer('status')->default('1');
            $table->string('phone')->nullable();
            $table->integer('is_email_sent')->default('0');
            $table->integer('is_email_verified')->default('0');
            $table->string('email_token')->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('country')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
