<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryrevisionsTable extends Migration
{

    public function up()
    {
        Schema::create('delivery_revisions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_order_message_id');
            $table->string('revision_status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_revisions');
    }
}
