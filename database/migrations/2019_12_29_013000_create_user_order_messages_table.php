<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserordermessagesTable extends Migration
{

    public function up()
    {
        Schema::create('user_order_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('order_id');
            $table->binary('user_message');
            $table->string('file_name', 191);
            $table->string('type', 20);
            $table->string('message_type', 191);
            $table->string('original_file_name', 191);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('user_order_messages');
    }
}
