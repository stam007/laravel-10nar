

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceimagesTable extends Migration
{

    public function up()
    {
        Schema::create('service_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->string('image_name', 191);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('service_images');
    }
}
