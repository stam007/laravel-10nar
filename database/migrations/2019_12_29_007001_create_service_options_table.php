

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceoptionsTable extends Migration
{

    public function up()
    {
        Schema::create('service_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->string('title', 191);
            $table->string('price', 191);
            $table->string('extra_delivery_delivery', 191);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('service_options');
    }
}
