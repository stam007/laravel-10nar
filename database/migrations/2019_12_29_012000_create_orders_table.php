

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{

    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seller_id');
            $table->integer('buyer_id');
            $table->integer('service_id');
            $table->integer('taken');
            $table->integer('seller_status');
            $table->integer('buyer_status');
            $table->integer('status');
            $table->double('money', 8, 3);
            $table->binary('rating');
            $table->integer('like');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
