<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawmethodsTable extends Migration
{

    public function up()
    {
        Schema::create('withdraw_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('min_limit', 10, 3);
            $table->double('max_limit', 10, 3);
            $table->double('fixed_charge', 8, 3);
            $table->double('percentage_charge', 5, 2);
            $table->string('process_time', 40);
            $table->integer('deleted')->default('0');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('withdraw_methods');
    }
}
