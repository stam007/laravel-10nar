<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysTable extends Migration
{

    public function up()
    {
        Schema::create('gateways', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->string('name');
            $table->string('gateimg');
            $table->double('minamo');
            $table->double('maxamo');
            $table->double('chargefx');
            $table->double('chargepc');
            $table->string('rate');
            $table->string('val1');
            $table->string('val2');
            $table->string('val3');
            $table->string('currency');
            $table->integer('status');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('gateways');
    }
}
