<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryacceptancesTable extends Migration
{

    public function up()
    {
        Schema::create('delivery_acceptances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_order_message_id');
            $table->string('acceptance_status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_acceptances');
    }
}

