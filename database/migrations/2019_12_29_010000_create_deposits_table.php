<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration
{

    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('gateway_id');
            $table->double('amount');
            $table->integer('status');
            $table->string('trx', 30);
            $table->string('bcam');
            $table->string('bcid');
            $table->integer('try');
            $table->string('wc_amount');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
