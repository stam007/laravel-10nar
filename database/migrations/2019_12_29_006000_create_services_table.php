<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{

    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('service_title');
            $table->binary('description');
            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->string('max_days_to_complete');
            $table->string('max_revisions');
            $table->string('video_url')->nullable();
            $table->string('slug'); 
            $table->integer('status')->default('1');
            $table->integer('show')->default('1');
            $table->integer('feature')->default('0');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('services');
    }
}

