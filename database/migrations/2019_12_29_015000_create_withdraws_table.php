

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *CREATE TABLE `withdraws` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `withdraw_method_id` int(11) DEFAULT NULL,
  `trx` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `charge` double(10,2) DEFAULT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `message` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
     * @return void
     */
    public function up()
    {
        Schema::create('withdraws', function (Blueprint $table) {
        // ["id","int(10) unsigned",null,"NO","PRI",null,"auto_increment","select,insert,update,references",""]
            $table->increments('id');
        // ["withdraw_method_id","int(11)",null,"YES","",null,"","select,insert,update,references",""]
            $table->integer('withdraw_method_id');
        // ["trx","varchar(50)","utf8mb4_unicode_ci","YES","",null,"","select,insert,update,references",""]
            $table->string('trx', 50);
        // ["user_id","int(11)",null,"YES","",null,"","select,insert,update,references",""]
            $table->integer('user_id');
        // ["amount","double(10,2)",null,"YES","",null,"","select,insert,update,references",""]
            $table->double('amount', 10, 2);
        // ["charge","double(10,2)",null,"YES","",null,"","select,insert,update,references",""]
            $table->double('charge', 10, 2);
        // ["status","varchar(30)","utf8mb4_unicode_ci","YES","",null,"","select,insert,update,references",""]
            $table->string('status', 30);
        // ["details","text","utf8mb4_unicode_ci","YES","",null,"","select,insert,update,references",""]
            $table->text('details');
        // ["message","blob",null,"YES","",null,"","select,insert,update,references",""]
            $table->binary('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraws');
    }
}

