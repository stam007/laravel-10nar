<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralsettingTable extends Migration
{
    public function up()
    {
        Schema::create('general_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rate');
            $table->string('website_title', 200);
            $table->string('base_curr_text', 20);
            $table->string('base_curr_symbol', 20);
            $table->string('registration', 10);
            $table->string('email_verification', 10);
            $table->string('sms_verification', 10);
            $table->string('decimal_after_pt', 10);
            $table->string('email_notification', 10);
            $table->string('sms_notification', 10);
            $table->string('email_sent_from', 255);
            $table->binary('email_template');
            $table->text('sms_api');
            $table->double('ref_com', 8, 2)->default('0.00');
            $table->string('phone', 191);
            $table->string('email', 191);
            $table->text('comment_script');
            $table->binary('tos');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('general_setting');
    }
}
